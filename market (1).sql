-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2020 at 09:13 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `market`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `ID` int(11) NOT NULL,
  `cat_Name` varchar(256) NOT NULL,
  `primary_Category` enum('Basic Necessities','Prime Commodities') NOT NULL DEFAULT 'Basic Necessities'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`ID`, `cat_Name`, `primary_Category`) VALUES
(500, 'Canned Sardines in tomato sauce', 'Basic Necessities'),
(501, 'Processed Milk', 'Basic Necessities'),
(502, 'Coffee Refill', 'Basic Necessities'),
(517, 'DISTILLED WATER', 'Basic Necessities'),
(504, 'Bread', 'Basic Necessities'),
(505, 'Luncheon Meat', 'Prime Commodities'),
(506, 'Meat Loaf', 'Prime Commodities'),
(507, 'Corned Beef', 'Prime Commodities'),
(508, 'RICE', 'Prime Commodities'),
(509, 'FISH', 'Prime Commodities'),
(510, 'MEAT', 'Prime Commodities'),
(511, 'POULTRY', 'Prime Commodities'),
(512, 'EGG', 'Prime Commodities'),
(513, 'Instant Noodles', 'Basic Necessities'),
(514, 'SALT (IODIZED)', 'Basic Necessities'),
(515, 'COFFEE 3-1 ORIGINAL', 'Basic Necessities'),
(516, 'PURIFIED WATER', 'Basic Necessities'),
(518, 'MINERALIZED WATER', 'Basic Necessities'),
(519, 'CANDLES', 'Basic Necessities'),
(520, 'FRUITS', 'Basic Necessities'),
(521, 'Condiments', 'Prime Commodities'),
(522, 'SUGAR', 'Basic Necessities'),
(523, 'Seaweeds', 'Prime Commodities'),
(524, 'Shells', 'Prime Commodities'),
(525, 'VEGETABLES', 'Prime Commodities');

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE `keywords` (
  `prd_ID` int(11) DEFAULT NULL,
  `keyword` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`prd_ID`, `keyword`) VALUES
(1010, 'Kape'),
(1013, 'Tinapay'),
(1013, 'Slice bread'),
(1019, 'De Lata'),
(1034, 'Bunay'),
(1034, 'Itlog'),
(1034, 'Salag'),
(1008, 'Condensada'),
(1008, 'Kondensada'),
(1008, 'Gatas'),
(1009, 'Condensada'),
(1009, 'Kondensada'),
(1009, 'Gatas'),
(1018, 'De Lata'),
(1045, 'Noodles'),
(1046, 'Noodles'),
(1047, 'Noodles'),
(1048, 'Noodles'),
(1049, 'Noodles'),
(1050, 'Tinapay. Pandisal'),
(1050, 'Pandesal'),
(1006, 'Condensada'),
(1006, 'Gatas'),
(1006, 'Kondensada'),
(1051, 'Alamang'),
(1051, 'Bagoong'),
(1051, 'Balaw'),
(1060, 'Ham'),
(1061, 'Bacon'),
(1061, 'Karne'),
(1062, 'Karne'),
(1062, 'Lamb'),
(1063, 'Karne'),
(1063, 'Turkey Meat'),
(1073, 'Pork Lucnheonmeat'),
(1081, 'Highland'),
(1081, 'cornedbeef'),
(1082, 'Delimondo'),
(1082, 'cornedbeef'),
(1094, 'Arla'),
(1094, 'gatas'),
(1094, 'milk'),
(1095, 'Nestle'),
(1095, 'gatas'),
(1095, 'milk'),
(1096, 'Anchor'),
(1096, 'gatas'),
(1096, 'mlik'),
(1097, 'Selecta'),
(1097, 'gatas'),
(1097, 'milk'),
(1098, 'Magnolla'),
(1098, 'gatas'),
(1098, 'milk'),
(1099, 'Anmum'),
(1099, 'gatas'),
(1099, 'milk'),
(1011, 'Kape'),
(1100, 'Coffee Decaf'),
(1100, 'coffee'),
(1100, 'kapi'),
(1100, 'capi'),
(1101, 'Pioir coffee'),
(1101, 'coffee'),
(1101, 'kapi'),
(1101, 'capi'),
(1102, 'Coffee Bags'),
(1102, 'coffee'),
(1102, 'kapi'),
(1102, 'capi'),
(1104, 'Jimms Black Coffee'),
(1104, 'coffee'),
(1104, 'kapi'),
(1104, 'capi'),
(1108, 'Spanish bread'),
(1108, 'tinapay'),
(1108, 'limbok'),
(1109, 'Ensaymada'),
(1109, 'tinapay'),
(1109, 'bread'),
(1110, 'Chesse bread'),
(1110, 'tinapay'),
(1110, 'bread'),
(1111, 'Banana bread'),
(1111, 'tinapay'),
(1111, 'bread'),
(1112, 'Yema Bread Rolls'),
(1112, 'tinapay'),
(1112, 'bread'),
(1113, 'Vegan Pandesal'),
(1113, 'pandesal'),
(1113, 'tinapay'),
(1113, 'bread'),
(1114, 'Monngo loaf bread'),
(1114, 'tinapay'),
(1114, 'bread'),
(1115, 'Monay'),
(1115, 'tinapay'),
(1115, 'bread'),
(1116, 'Lucky me supreme seafood'),
(1117, 'Lucky me supreme pinoy checken'),
(1118, 'Quickchow pancicanton'),
(1119, 'Quickchow bihin guisado'),
(1120, 'Lucky me supreme bulalo'),
(1003, 'De Lata'),
(1003, 'Sardinas'),
(1003, 'Delata'),
(1003, 'Dilata'),
(1004, 'De Lata'),
(1004, 'Sardinas'),
(1004, 'Delata'),
(1004, 'Dilata'),
(1005, 'De Lata'),
(1005, 'Sardinas'),
(1005, 'Delata'),
(1005, 'Dilata'),
(1035, 'De Lata'),
(1035, 'Sardinas'),
(1035, 'Delata'),
(1035, 'Dilata'),
(1036, 'De Lata'),
(1036, 'Sardinas'),
(1036, 'Delata'),
(1036, 'Dilata'),
(1037, 'De Lata'),
(1037, 'Sardinas'),
(1037, 'Delata'),
(1037, 'Dilata'),
(1038, 'De Lata'),
(1038, 'Sardinas'),
(1038, 'Delata'),
(1038, 'Dilata'),
(1039, 'De Lata'),
(1039, 'Sardinas'),
(1039, 'Delata'),
(1039, 'Dilata'),
(1040, 'De Lata'),
(1040, 'Sardinas'),
(1040, 'Delata'),
(1040, 'Dilata'),
(1041, 'De Lata'),
(1041, 'Sardinas'),
(1041, 'Delata'),
(1041, 'Dilata'),
(1105, '3 and 1'),
(1105, 'capi'),
(1105, 'coffee'),
(1105, 'kapi'),
(1105, 'kopiko Blanca'),
(1105, '3and1'),
(1106, '3and1'),
(1106, '3 and 1'),
(1106, 'Kopiko Brown'),
(1106, 'capi'),
(1106, 'coffee'),
(1106, 'kapi'),
(1016, 'De Lata'),
(1016, 'Dilata'),
(1016, 'Delata'),
(1017, 'De Lata'),
(1017, 'Dilata'),
(1017, 'Delata'),
(1042, 'De Lata'),
(1042, 'Dilata'),
(1042, 'Delata'),
(1043, 'De Lata'),
(1043, 'Dilata'),
(1043, 'Delata'),
(1044, 'De Lata'),
(1044, 'Dilata'),
(1044, 'Delata'),
(1076, 'hilips'),
(1076, 'meatloaf'),
(1076, 'Delata'),
(1076, 'De Lata'),
(1076, 'Dilata'),
(1077, 'De Lata'),
(1077, 'Dilata'),
(1077, 'Delata'),
(1077, 'Meatloaf'),
(1078, 'De Lata'),
(1078, 'Dilata'),
(1078, 'Delata'),
(1078, 'Meatloaf'),
(1079, 'De Lata'),
(1079, 'Delata'),
(1079, 'Dilata'),
(1079, 'Meatloaf'),
(1080, 'De Lata'),
(1080, 'Dilata'),
(1080, 'Delata'),
(1080, 'Meatloaf'),
(1014, 'De Lata'),
(1014, 'Dilata'),
(1014, 'Delata'),
(1014, 'Luncheon Meat'),
(1015, 'De Lata'),
(1015, 'Dilata'),
(1015, 'Delata'),
(1015, 'Luncheon Meat'),
(1068, 'Jamonilla'),
(1068, 'De Lata'),
(1068, 'Delata'),
(1068, 'Dilata'),
(1068, 'Tulip'),
(1069, 'Jamonilla'),
(1069, 'De Lata'),
(1069, 'Delata'),
(1069, 'Dilata'),
(1069, 'Maling'),
(1070, 'Shanghai Launcheon meat'),
(1070, 'Delata'),
(1070, 'De Lata'),
(1070, 'Dilata'),
(1070, 'Luncheon Meat'),
(1071, 'Holiday Lauchneon meat'),
(1071, 'De Lata'),
(1071, 'Delata'),
(1071, 'Dilata'),
(1071, 'Luncheon Meat'),
(1072, 'Philips'),
(1072, 'De Lata'),
(1072, 'Delata'),
(1072, 'Dilata'),
(1074, 'Salland'),
(1074, 'De Lata'),
(1074, 'Delata'),
(1074, 'Dilata'),
(1020, 'Bigas'),
(1020, 'Bugas'),
(1020, 'Bagas'),
(1021, 'Bigas'),
(1021, 'Bugas'),
(1021, 'Bagas'),
(1022, 'Bigas'),
(1022, 'Bugas'),
(1022, 'Bagas'),
(1023, 'Bigas'),
(1023, 'Bugas'),
(1023, 'Bagas'),
(1024, 'Bigas'),
(1024, 'Bugas'),
(1024, 'Bagas'),
(1089, 'Bigas'),
(1089, 'Bugas'),
(1089, 'Doña Maria Jasponica Brown'),
(1089, 'Rice'),
(1089, 'Bagas'),
(1090, 'GohanLite lite'),
(1090, 'bigas'),
(1090, 'bugas'),
(1090, 'Bagas'),
(1091, 'Doña Maria Jasponica White Rice'),
(1091, 'bigas'),
(1091, 'bugas'),
(1091, 'Bagas'),
(1092, 'Jordan Farms’ Authentic Basmati Rice'),
(1092, 'bigas'),
(1092, 'bugas'),
(1092, 'Bagas'),
(1093, 'Harvester’s Dinorado Rice'),
(1093, 'bigas'),
(1093, 'bugas'),
(1093, 'Bagas'),
(1026, 'Isda'),
(1026, 'Sera'),
(1026, 'Sira'),
(1026, 'Tilapya'),
(1026, 'Fish'),
(1025, 'Bangus'),
(1025, 'Isda'),
(1025, 'Sera'),
(1025, 'Sira'),
(1025, 'Fish'),
(1027, 'Isda'),
(1027, 'Sera'),
(1027, 'Sibubog'),
(1027, 'Sira'),
(1027, 'Fish'),
(1027, 'Galunggong'),
(1028, 'Alumahan'),
(1028, 'Fish'),
(1028, 'Isda Sera'),
(1028, 'Sira'),
(1054, 'Banagan'),
(1054, 'Buyod'),
(1054, 'Hipon'),
(1055, 'Fish'),
(1055, 'Isda'),
(1055, 'Sera'),
(1055, 'Sira'),
(1055, 'Dalagang bukid'),
(1056, 'Fish'),
(1056, 'Sera'),
(1056, 'Sira'),
(1056, 'Isda'),
(1056, 'Bisugo'),
(1029, 'Baka'),
(1029, 'Karne'),
(1029, 'Karni'),
(1030, 'Baka'),
(1030, 'Karne'),
(1030, 'Karni'),
(1031, 'Baboy'),
(1031, 'Karne'),
(1031, 'Karni'),
(1031, 'Laman'),
(1032, 'Baboy'),
(1032, 'Karne'),
(1032, 'Karni'),
(1032, 'Laman'),
(1032, 'Liempo'),
(1032, 'Pork Liempo'),
(1058, 'Karne'),
(1058, 'Steak'),
(1058, 'Baboy'),
(1058, 'Karni'),
(1058, 'Karning Baboy'),
(1058, 'Laman'),
(1058, 'Taba'),
(1059, 'Karne'),
(1059, 'Ribs'),
(1059, 'Karni'),
(1059, 'Baboy'),
(1064, 'Manok'),
(1064, 'Karning Manok'),
(1064, 'Chicken'),
(1064, 'Wings'),
(1065, 'Manok'),
(1065, 'Karning Manok'),
(1065, 'Chicken'),
(1066, 'Duck'),
(1066, 'Pato'),
(1125, 'Vinegar'),
(1125, 'Suka'),
(1125, 'Soka'),
(1126, 'Patis'),
(1126, 'Pates'),
(1127, 'Taoyo'),
(1127, 'Toyo'),
(1127, 'Tawyo'),
(1127, 'Soy Sauce'),
(1128, 'Taoyo'),
(1128, 'Toyo'),
(1128, 'Tawyo'),
(1128, 'Soy Sauce'),
(1129, 'Patis'),
(1129, 'Pates'),
(1130, 'Suka'),
(1130, 'Soka'),
(1130, 'Vinegar'),
(1131, 'Refined Sugar'),
(1131, 'Sugar'),
(1131, 'Asukar'),
(1131, 'Asokar'),
(1131, 'Asukal'),
(1132, 'Washed Sugar'),
(1132, 'Sugar'),
(1132, 'Asukar'),
(1132, 'Asokar'),
(1132, 'Asukal'),
(1133, 'Brown Sugar'),
(1133, 'Sugar'),
(1133, 'Asukal'),
(1133, 'Asokar'),
(1133, 'Asukal'),
(1134, 'Hito'),
(1134, 'Fish'),
(1134, 'Isda'),
(1135, 'Lapu-lapu'),
(1135, 'Fish'),
(1135, 'Isda'),
(1135, 'Sera'),
(1135, 'Sira'),
(1136, 'Fish'),
(1136, 'Isda'),
(1136, 'Matambaka'),
(1136, 'Sera'),
(1136, 'Sira'),
(1137, 'Maya-Maya'),
(1137, 'Fish'),
(1137, 'Isda'),
(1137, 'Sira'),
(1137, 'Sera'),
(1138, 'Pusit'),
(1139, 'Sapsap Fish'),
(1139, 'Fish'),
(1139, 'Sira'),
(1139, 'Sera'),
(1139, 'Isda'),
(1140, 'Tahong'),
(1140, 'Shell'),
(1141, 'Talaba'),
(1141, 'Shell'),
(1142, 'Tamban Fish'),
(1142, 'Tamban'),
(1142, 'Fish'),
(1142, 'Sira'),
(1142, 'Sera'),
(1142, 'Isda'),
(1143, 'Tanigue fish'),
(1143, 'Tanigue'),
(1143, 'Fish'),
(1143, 'Sira'),
(1143, 'Sera'),
(1143, 'Isda'),
(1144, 'Tulingan'),
(1144, 'Fish'),
(1144, 'Sera'),
(1144, 'Sira'),
(1144, 'Isda'),
(1145, 'Tambakol'),
(1145, 'Fish'),
(1145, 'Sira'),
(1145, 'Sera'),
(1145, 'Isda'),
(1147, 'Amargoso'),
(1147, 'Ampalaya'),
(1147, 'Marigoso'),
(1147, 'Vegetable'),
(1147, 'Gulay'),
(1148, 'Antak'),
(1148, 'Gulay'),
(1148, 'Sitao'),
(1148, 'Sitaw'),
(1148, 'Vegetables'),
(1148, 'Gulay'),
(1149, 'Kamatis'),
(1149, 'Tomato'),
(1149, 'Vegetable'),
(1149, 'Gulay'),
(1150, 'Onion red'),
(1150, 'Sibulyas'),
(1150, 'Sibuyas'),
(1150, 'Vegetables'),
(1150, 'Gulay'),
(1151, 'Patatas'),
(1151, 'Potato'),
(1151, 'Vegetable'),
(1151, 'Gulay'),
(1152, 'Eggplant'),
(1152, 'Talong'),
(1152, 'Vegetable'),
(1152, 'Gulay'),
(1153, 'Cabbage'),
(1153, 'Repolyo'),
(1153, 'Vegetable'),
(1153, 'Gulay'),
(1121, 'Banana'),
(1121, 'Batag'),
(1121, 'Lakatan'),
(1121, 'Fruits'),
(1121, 'Prutas'),
(1121, 'Protas'),
(1122, 'Banana'),
(1122, 'Batag'),
(1122, 'Latundan'),
(1122, 'Saging'),
(1122, 'Fruits'),
(1122, 'Prutas'),
(1122, 'Protas'),
(1123, 'Papaya'),
(1123, 'Tapayas'),
(1123, 'Fruits'),
(1123, 'Prutas'),
(1123, 'Protas'),
(1124, 'Carabao Mango'),
(1124, 'Manga'),
(1124, 'Mangga'),
(1124, 'Mango'),
(1124, 'Fruits'),
(1124, 'Prutas'),
(1124, 'Protas'),
(1146, 'Calamansi'),
(1146, 'Suha'),
(1146, 'Suwa'),
(1146, 'Fruits'),
(1146, 'Prutas'),
(1146, 'Protas'),
(1033, 'Chicken'),
(1033, 'Karning Manok'),
(1033, 'Manok'),
(1007, 'Condensada'),
(1007, 'Gatas'),
(1007, 'Kondensada'),
(1001, 'De Lata'),
(1001, 'Delata'),
(1001, 'Dilata'),
(1001, 'Sardinas'),
(1107, '3 and 1'),
(1107, '3and1'),
(1107, 'Sanmig Coffee'),
(1107, 'capi'),
(1107, 'coffee'),
(1107, 'kapi'),
(1002, 'De Lata'),
(1002, 'Delata'),
(1002, 'Dilata'),
(1002, 'Sardinas'),
(1000, 'De Lata'),
(1000, 'Delata'),
(1000, 'Dilata'),
(1000, 'Sardinas');

-- --------------------------------------------------------

--
-- Table structure for table `log_complain`
--

CREATE TABLE `log_complain` (
  `ID` int(11) NOT NULL,
  `log_Date` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL DEFAULT 'Unknown',
  `contact` varchar(255) NOT NULL DEFAULT '09123456789',
  `address` varchar(255) NOT NULL DEFAULT 'Daraga',
  `category` varchar(255) NOT NULL DEFAULT 'category 1',
  `shop` varchar(255) NOT NULL,
  `message` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log_complain`
--

INSERT INTO `log_complain` (`ID`, `log_Date`, `name`, `contact`, `address`, `category`, `shop`, `message`) VALUES
(10003, '2020-01-13 08:40:25', 'Renche', '09952959379', 'Anislag', 'Category 1', 'LCC', 'Mahal ang bakalon'),
(10009, '2020-02-02 01:10:23', 'Remond Alejo', '09125035965', 'Daraga', 'Category 1', 'Dan &amp; Amy Verdillo Meat Shop', 'Mahal po ang mga bakalon.'),
(10010, '2020-02-02 01:52:56', 'test', '09503064148', 'Daraga', 'Category 1', 'Napay Meat Shop', 'test lang'),
(10012, '2020-02-05 08:05:24', 'Testing', '09123456789', 'tesfsdfdsfs', 'Fruits Section', 'Boboy Store', 'asdasdasdas');

-- --------------------------------------------------------

--
-- Table structure for table `log_notif`
--

CREATE TABLE `log_notif` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `log_price_update`
--

CREATE TABLE `log_price_update` (
  `log_Date` timestamp NOT NULL DEFAULT current_timestamp(),
  `prd_ID` int(11) NOT NULL,
  `old_price` double(20,2) NOT NULL,
  `new_price` double(20,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_price_update`
--

INSERT INTO `log_price_update` (`log_Date`, `prd_ID`, `old_price`, `new_price`) VALUES
('2019-11-04 01:56:31', 1000, 15.50, 12.50),
('2019-11-04 01:59:37', 1002, 17.25, 15.25),
('2019-11-06 00:01:49', 1000, 12.50, 12.70),
('2019-11-06 01:17:11', 1025, 160.00, 170.00),
('2019-11-06 01:21:50', 1027, 160.00, 165.00),
('2019-11-06 02:20:14', 1006, 57.00, 59.00),
('2019-11-06 06:45:38', 1000, 12.70, 16.00),
('2019-11-06 06:48:49', 1001, 15.90, 14.90),
('2019-11-15 13:31:07', 1000, 16.00, 17.00),
('2019-11-15 14:40:46', 1001, 14.90, 14.70),
('2020-01-14 07:41:23', 1000, 17.00, 16.00),
('2020-01-20 08:58:36', 1000, 16.00, 17.00),
('2020-01-20 09:19:37', 1001, 14.70, 14.50),
('2020-01-20 18:05:07', 1004, 13.25, 14.25),
('2020-01-21 06:13:36', 1000, 17.00, 18.00),
('2020-01-30 13:49:17', 1033, 170.00, 171.00),
('2020-01-30 13:50:36', 1033, 171.00, 190.00),
('2020-01-30 14:05:31', 1033, 190.00, 120.00),
('2020-01-30 14:15:09', 1033, 120.00, 157.00),
('2020-01-31 08:20:26', 1000, 18.00, 19.00),
('2020-01-31 08:20:52', 1000, 19.00, 15.00),
('2020-02-02 00:41:06', 1000, 15.00, 17.00),
('2020-02-02 01:13:34', 1007, 72.00, 62.00),
('2020-02-02 04:53:19', 1000, 17.00, 18.00),
('2020-02-02 05:11:37', 1001, 14.50, 14.51),
('2020-02-02 05:20:30', 1107, 70.00, 71.00),
('2020-02-05 08:00:44', 1000, 18.00, 15.00),
('2020-02-05 08:01:09', 1000, 15.00, 18.00);

-- --------------------------------------------------------

--
-- Table structure for table `numbers`
--

CREATE TABLE `numbers` (
  `number` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `numbers`
--

INSERT INTO `numbers` (`number`) VALUES
('09125035965'),
('09304838481');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ID` int(11) NOT NULL,
  `prd_Name` varchar(255) NOT NULL,
  `prd_Local` varchar(255) DEFAULT NULL,
  `prd_Details` varchar(5000) DEFAULT NULL,
  `prd_Category` int(11) NOT NULL,
  `prd_Price` double(20,2) NOT NULL,
  `prd_Unit` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ID`, `prd_Name`, `prd_Local`, `prd_Details`, `prd_Category`, `prd_Price`, `prd_Unit`) VALUES
(1000, '555 Bonus Pack', '', '', 500, 18.00, '155g'),
(1001, 'Atami Blue', '', '', 500, 14.51, '155g'),
(1002, 'Family\'s Bonus Pack Plain', '', '', 500, 15.25, '158g'),
(1003, 'Family\'s Budget Pack Plain', '', '', 500, 15.25, '130g'),
(1004, 'Young\'s Town Bonus', '', '', 500, 14.25, '155g'),
(1005, 'Toyo Green Easy Open', '', '', 500, 16.50, '155g'),
(1006, 'Alaska Sweetened Filled Milk', '', 'CONDENSED MILK', 501, 59.00, '300mL'),
(1007, 'Milkmaid Full Cream Milk', '', 'CONDENSED MILK', 501, 62.00, '300mL'),
(1008, 'Alaska', '', 'CONDENSADA', 501, 43.50, '300mL'),
(1009, 'Carnation', '', 'CONDENSADA', 501, 54.50, '300mL'),
(1010, 'Nescafe (Classic)', '', '', 502, 19.70, '25g'),
(1011, 'Nescafe (Classic) double filter', '', '', 502, 39.40, '50g'),
(1051, 'Alamang', '', '', 509, 150.00, 'per kg'),
(1013, 'Pinoy Tasty', '', '', 504, 35.00, '450g'),
(1014, 'CDO', '', '', 505, 32.50, '165g'),
(1015, 'Purefoods Chinese Style', '', '', 505, 32.50, '165g'),
(1016, '555', '', '', 506, 17.70, '150g'),
(1017, 'Argentina', '', '', 506, 18.30, '150g'),
(1018, 'Argentina', '', '', 507, 32.00, '150g'),
(1019, 'Bingo', '', '', 507, 18.50, '150g'),
(1020, 'NFA/Regular Milled Rice', '', 'Regular Milled', 508, 27.00, 'per kg'),
(1021, 'NFA/Well Milled Rice', '', '', 508, 32.00, 'per kg'),
(1022, 'Commercial/Regular Milled Rice', '', 'Regular Milled', 508, 35.00, 'per kg'),
(1023, 'Commercial/Well Milled Rice', '', '', 508, 44.00, 'per kg'),
(1024, 'Commercial/Premium Rice', '', '', 508, 46.00, 'per kg'),
(1025, 'Bangus', '', '', 509, 170.00, 'per kg'),
(1026, 'Tilapia', '', '', 509, 120.00, 'per kg'),
(1027, 'Galunggong', '', '', 509, 165.00, 'per kg'),
(1028, 'Alumahan', '', '', 509, 260.00, 'per kg'),
(1029, 'Beef rump', '', '', 510, 360.00, 'per kg'),
(1030, 'Beef brisket', '', '', 510, 300.00, 'per kg'),
(1031, 'Pork ham/kasim', '', '', 510, 220.00, 'per kg'),
(1032, 'Pork liempo', '', '', 510, 230.00, 'per kg'),
(1033, 'Chicken', '', '', 511, 157.00, 'Whole Chicken'),
(1034, 'Chicken egg', 'Egg', '', 511, 7.00, 'med/per piece'),
(1035, 'Mikado', '', '', 500, 14.50, '155g'),
(1036, 'Hakone Regular', '', '', 500, 13.40, '155g'),
(1037, 'Rose Bowl', '', 'Red', 500, 16.40, '155g'),
(1038, 'Rose Bowl', '', 'Green', 500, 16.40, '155g'),
(1039, 'Saba', '', 'Red', 500, 15.80, '155g'),
(1040, 'Saba', '', 'Green', 500, 15.80, '155g'),
(1041, 'Toyo Green', '', '', 500, 15.50, '155g'),
(1042, 'CDO', '', '', 506, 16.95, '150g'),
(1043, 'Winner', '', 'Supermarket', 506, 16.50, '150g'),
(1044, 'Argentina', '', '', 506, 19.80, '170g'),
(1045, 'Lucky Me (Beef)', '', '', 513, 7.30, '55g'),
(1046, 'Ho-mi', '', '', 513, 7.20, '55g'),
(1047, 'Lucky Me (Chicken)', '', '', 513, 7.30, '55g'),
(1048, 'Quickchow (Beef)', '', '', 513, 6.60, '55g'),
(1049, 'Quickchow (Hot Beef)', '', '', 513, 6.60, '55g'),
(1050, 'Pinoy Pandesal', '', '10 pcs/ pack', 504, 21.50, '250g'),
(1052, 'Alimango', '', '', 509, 500.00, 'per kg'),
(1053, 'Alumahan', '', '', 509, 180.00, 'per kg'),
(1054, 'Banagan', '', '', 509, 340.00, 'kg'),
(1055, 'Dalagang Bukid', '', '', 509, 140.00, 'kg'),
(1056, 'Bisugo', '', '', 509, 180.00, 'kg'),
(1125, 'Vinegar', '', '', 521, 6.40, '200 ml'),
(1058, 'Steak', '', '', 510, 340.00, 'kg'),
(1059, 'Ribs', '', '', 510, 160.00, 'kg'),
(1060, 'Ham', '', '', 510, 599.00, 'kg'),
(1061, 'Bacon', '', '', 510, 450.00, 'kg'),
(1062, 'Lamb', '', '', 510, 600.00, 'kg'),
(1063, 'Turkey Meat', '', '', 510, 700.00, 'kg'),
(1064, 'Checken wings meat', '', '', 511, 180.00, 'kg'),
(1065, 'chicken leg meat', '', '', 511, 180.00, 'kg'),
(1066, 'Duck Meat', '', '', 511, 600.00, 'kg'),
(1068, 'Jamonilla', '', '', 505, 56.00, 'per pcs'),
(1069, 'Maling', '', '', 505, 67.00, 'per pcs'),
(1070, 'Shanghai', '', '', 505, 45.00, 'per pcs'),
(1071, 'Holiday Luchneon meat', '', '', 505, 44.00, 'per pcs'),
(1072, 'Philips', '', '', 505, 34.00, 'per pcs'),
(1073, 'Pork Lucnheonmeat', '', '', 505, 68.00, 'Kilogram'),
(1074, 'Salland', '', '', 505, 56.00, 'Kilogram'),
(1075, 'Durra', '', '', 505, 56.00, 'kg'),
(1076, 'philips meatloaf', '', '', 506, 33.00, 'per pcs'),
(1077, 'Star Meatloaf', '', '', 506, 18.00, 'per pcs'),
(1078, 'Holiday meatloaf', '', '', 506, 20.00, 'per pcs'),
(1079, 'Prem meatloaf', '', '', 506, 32.00, 'per pcs'),
(1080, 'Binggo meatloaf', '', '', 506, 18.00, 'per pcs'),
(1081, 'Highland', '', '', 507, 19.00, 'per pcs'),
(1082, 'Delimondo', '', '', 507, 90.00, 'per pcs'),
(1083, 'beeloaf', '', '', 507, 17.00, 'per pcs'),
(1084, 'libby Cornedbeef', '', '', 507, 45.00, 'per pcs'),
(1085, 'Australlian', '', '', 507, 80.00, 'per pcs'),
(1086, 'Hereford', '', '', 507, 105.00, '155g'),
(1087, 'Target extra', '', '', 507, 45.00, 'per pcs'),
(1088, 'Palm', '', '', 507, 80.00, 'per pcs'),
(1089, 'Doña Maria Jasponica Brown', '', '', 508, 56.00, 'per kg'),
(1090, 'GohanLite lite', '', '', 508, 89.00, 'per kg'),
(1091, 'Doña Maria Jasponica White Rice', '', '', 508, 67.00, 'per kg'),
(1092, 'Jordan Farms’ Authentic Basmati Rice', '', '', 508, 76.00, 'per kg'),
(1093, 'Harvester’s Dinorado Rice', '', '', 508, 89.00, 'per kg'),
(1094, 'Arla', '', '', 501, 35.00, 'per ML'),
(1095, 'Nestle', '', '', 501, 50.00, 'per ML'),
(1096, 'Anchor', '', '', 501, 35.00, 'per ML'),
(1097, 'Selecta', '', '', 501, 28.00, 'per ML'),
(1098, 'Magnolla', '', '', 501, 68.00, 'perML'),
(1099, 'Anmum', '', '', 501, 45.00, 'per ML'),
(1100, 'Coffee Decaf', '', '', 502, 56.00, 'per ML'),
(1101, 'Pioir coffee', '', '', 502, 35.00, 'per ML'),
(1102, 'Coffee Bags', '', '', 502, 67.00, 'per ML'),
(1103, 'Jimms Coffee', '', '', 502, 56.00, 'per ML'),
(1104, 'Jimms Black Coffee', '', '', 502, 30.00, '155g'),
(1105, 'kopiko Blanca', '', '', 502, 56.00, '155g'),
(1106, 'Kopiko Brown', '', '', 502, 56.00, '155g'),
(1107, 'Sanmig Coffee', '', '', 502, 71.00, '155g'),
(1108, 'Spanish bread', '', '', 504, 5.00, 'per pcs'),
(1109, 'Ensaymada', '', '', 504, 5.00, 'per pcs'),
(1110, 'Chesse bread', '', '', 504, 2.50, 'per pcs'),
(1111, 'Banana bread', '', '', 504, 35.00, 'per pcs'),
(1112, 'Yema Bread Rolls', '', '', 504, 5.00, 'per pcs'),
(1113, 'Vegan Pandesal', '', '', 504, 5.00, 'per pcs'),
(1114, 'Monngo loaf bread', '', '', 504, 5.00, 'per pcs'),
(1115, 'Monay', '', '', 504, 5.00, 'per pcs'),
(1116, 'Lucky me supreme seafood', '', '', 513, 50.00, 'per pcs'),
(1117, 'Lucky me supreme pinoy checken', '', '', 513, 50.00, 'per pcs'),
(1118, 'Quickchow pancicanton', '', '', 513, 18.00, 'per pcs'),
(1119, 'Quickchow bihin guisado', '', '', 513, 18.00, 'per pcs'),
(1120, 'Lucky me supreme bulalo', '', '', 513, 19.00, 'per pcs'),
(1121, 'Lakatan', '', '', 520, 60.00, 'kg'),
(1122, 'Latundan', '', '', 520, 62.00, 'kg'),
(1123, 'Papaya', '', '', 520, 58.00, 'kg'),
(1124, 'Carabao Mango', '', '', 520, 49.00, 'kg'),
(1126, 'Patis', '', '', 521, 9.90, '150 ml'),
(1127, 'Soy Sauce', '', '', 521, 9.20, '200ml'),
(1128, 'Soy Sauce', '', '', 521, 17.40, '340ml'),
(1129, 'Patis', '', '', 521, 18.25, '350ml'),
(1130, 'Vinegar', '', '', 521, 14.95, '350mL'),
(1131, 'Refined Sugar', '', '', 522, 46.00, 'kg'),
(1132, 'Washed Sugar', '', '', 522, 37.00, 'kg'),
(1133, 'Brown Sugar', '', '', 522, 43.00, 'kg'),
(1134, 'Hito', '', '', 509, 100.00, 'kg'),
(1135, 'Lapu-lapu', '', '', 509, 176.67, 'kg'),
(1136, 'Matambaka', '', '', 509, 133.00, 'kg'),
(1137, 'Maya-Maya', '', '', 509, 177.50, 'kg'),
(1138, 'Pusit', '', '', 509, 104.43, 'kg'),
(1139, 'Sapsap', '', '', 509, 47.50, 'kg'),
(1140, 'Tahong', '', '', 524, 50.00, 'kg'),
(1141, 'Talaba', '', '', 524, 140.00, 'kg'),
(1142, 'Tamban', '', '', 509, 53.75, 'kg'),
(1143, 'Tanigue', '', '', 509, 190.00, 'kg'),
(1144, 'Tulingan', '', '', 509, 101.25, 'kg'),
(1145, 'Tambakol', '', '', 509, 145.00, 'kg'),
(1146, 'Calamansi', '', '', 520, 16.66, 'kg'),
(1147, 'Amargoso', '', '', 525, 70.00, 'kg'),
(1148, 'Sitao', '', '', 525, 45.71, '3-4 bundles/kg'),
(1149, 'Kamatis', '', '', 525, 32.00, '15-18 pcs/kg'),
(1150, 'Onion red', '', '', 525, 44.73, '13-15 pcs/kg'),
(1151, 'Potato', '', '', 525, 62.50, '10-12 pcs/kg'),
(1152, 'Eggplant', '', '', 525, 50.00, '8-10 pcs/kg'),
(1153, 'Cabbage', '', '', 525, 56.00, '750 gms- 1kg/head');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `prd_ID` int(11) NOT NULL,
  `img_Loc` varchar(5000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`prd_ID`, `img_Loc`) VALUES
(1000, 'prd_images/[1000][0].png'),
(1000, 'prd_images/[1000][1].jpg'),
(1001, 'prd_images/[1001][0].jpg'),
(1002, 'prd_images/[1002][0].png'),
(1003, 'prd_images/[1003][0].jpg'),
(1004, 'prd_images/[1004][0].png'),
(1004, 'prd_images/[1004][1].jpg'),
(1005, 'prd_images/[1005][0].jpg'),
(1005, 'prd_images/[1005][1].png'),
(1006, 'prd_images/[1006][0].png'),
(1007, 'prd_images/[1007][0].png'),
(1008, 'prd_images/[1008][0].png'),
(1009, 'prd_images/[1009][0].png'),
(1010, 'prd_images/[1010][0].jpg'),
(1011, 'prd_images/[1011][0].jpg'),
(1051, 'prd_images/[1051][0].jpg'),
(1013, 'prd_images/[1013][0].jpg'),
(1014, 'prd_images/[1014][0].png'),
(1015, 'prd_images/[1015][0].jpg'),
(1015, 'prd_images/[1015][1].jpg'),
(1016, 'prd_images/[1016][0].jpg'),
(1016, 'prd_images/[1016][1].jpg'),
(1017, 'prd_images/[1017][0].jpg'),
(1017, 'prd_images/[1017][1].png'),
(1018, 'prd_images/[1018][0].jpg'),
(1019, 'prd_images/[1019][0].png'),
(1022, 'prd_images/[1022][0].png'),
(1021, 'prd_images/[1021][0].jpg'),
(1020, 'prd_images/[1020][1].jpg'),
(1023, 'prd_images/[1023][0].jpg'),
(1024, 'prd_images/[1024][0].jpg'),
(1025, 'prd_images/[1025][0].jpg'),
(1026, 'prd_images/[1026][0].jpg'),
(1027, 'prd_images/[1027][0].png'),
(1028, 'prd_images/[1028][0].jpg'),
(1029, 'prd_images/[1029][0].png'),
(1030, 'prd_images/[1030][0].jpg'),
(1031, 'prd_images/[1031][0].png'),
(1032, 'prd_images/[1032][0].png'),
(1033, 'prd_images/[1033][0].jpg'),
(1034, 'prd_images/[1034][0].jpg'),
(1035, 'prd_images/[1035][0].jpg'),
(1036, 'prd_images/[1036][0].jpg'),
(1037, 'prd_images/[1037][0].jpg'),
(1038, 'prd_images/[1038][0].jpg'),
(1039, 'prd_images/[1039][0].jpg'),
(1040, 'prd_images/[1040][0].jpg'),
(1041, 'prd_images/[1041][0].png'),
(1042, 'prd_images/[1042][0].jpg'),
(1043, 'prd_images/[1043][0].png'),
(1044, 'prd_images/[1044][0].png'),
(1045, 'prd_images/[1045][0].png'),
(1046, 'prd_images/[1046][0].png'),
(1047, 'prd_images/[1047][0].png'),
(1048, 'prd_images/[1048][0].jpg'),
(1049, 'prd_images/[1049][0].jpg'),
(1050, 'prd_images/[1050][0].jpg'),
(1051, 'prd_images/[1051][1].jpg'),
(1051, 'prd_images/[1051][2].jpg'),
(1052, 'prd_images/[1052][0].jpg'),
(1052, 'prd_images/[1052][1].jpg'),
(1053, 'prd_images/[1053][0].jpg'),
(1053, 'prd_images/[1053][1].jpg'),
(1054, 'prd_images/[1054][0].jpg'),
(1054, 'prd_images/[1054][1].jpg'),
(1055, 'prd_images/[1055][0].jpg'),
(1055, 'prd_images/[1055][1].jpg'),
(1056, 'prd_images/[1056][0].jpg'),
(1056, 'prd_images/[1056][1].jpg'),
(1125, 'prd_images/[1125][0].png'),
(1058, 'prd_images/[1058][0].jpg'),
(1058, 'prd_images/[1058][1].png'),
(1059, 'prd_images/[1059][0].jpg'),
(1059, 'prd_images/[1059][1].jpg'),
(1060, 'prd_images/[1060][0].jpg'),
(1060, 'prd_images/[1060][1].jpg'),
(1061, 'prd_images/[1061][0].jpg'),
(1061, 'prd_images/[1061][1].jpg'),
(1062, 'prd_images/[1062][0].jpg'),
(1062, 'prd_images/[1062][1].jpg'),
(1063, 'prd_images/[1063][0].jpg'),
(1063, 'prd_images/[1063][1].jpg'),
(1064, 'prd_images/[1064][0].jpg'),
(1064, 'prd_images/[1064][1].jpg'),
(1065, 'prd_images/[1065][0].jpg'),
(1065, 'prd_images/[1065][1].jpg'),
(1066, 'prd_images/[1066][0].jpg'),
(1066, 'prd_images/[1066][1].jpg'),
(1068, 'prd_images/[1068][0].jpg'),
(1069, 'prd_images/[1069][0].jpg'),
(1070, 'prd_images/[1070][0].jpg'),
(1071, 'prd_images/[1071][0].jpg'),
(1072, 'prd_images/[1072][0].jpg'),
(1073, 'prd_images/[1073][0].jpg'),
(1074, 'prd_images/[1074][0].jpg'),
(1075, 'prd_images/[1075][0].jpg'),
(1076, 'prd_images/[1076][0].jpg'),
(1077, 'prd_images/[1077][0].jpg'),
(1078, 'prd_images/[1078][0].jpg'),
(1079, 'prd_images/[1079][0].jpg'),
(1080, 'prd_images/[1080][0].jpg'),
(1081, 'prd_images/[1081][0].jpg'),
(1082, 'prd_images/[1082][0].jpg'),
(1083, 'prd_images/[1083][0].jpg'),
(1084, 'prd_images/[1084][0].jpg'),
(1085, 'prd_images/[1085][0].jpg'),
(1086, 'prd_images/[1086][0].jpg'),
(1087, 'prd_images/[1087][0].jpg'),
(1088, 'prd_images/[1088][0].jpg'),
(1089, 'prd_images/[1089][0].jpg'),
(1090, 'prd_images/[1090][0].jpg'),
(1091, 'prd_images/[1091][0].jpg'),
(1092, 'prd_images/[1092][0].jpg'),
(1093, 'prd_images/[1093][0].jpg'),
(1094, 'prd_images/[1094][0].jpg'),
(1095, 'prd_images/[1095][0].jpg'),
(1096, 'prd_images/[1096][0].jpg'),
(1097, 'prd_images/[1097][0].jpg'),
(1098, 'prd_images/[1098][0].jpg'),
(1099, 'prd_images/[1099][0].jpg'),
(1100, 'prd_images/[1100][0].jpg'),
(1101, 'prd_images/[1101][0].jpg'),
(1102, 'prd_images/[1102][0].jpg'),
(1103, 'prd_images/[1103][0].jpg'),
(1104, 'prd_images/[1104][0].jpg'),
(1105, 'prd_images/[1105][0].jpg'),
(1106, 'prd_images/[1106][0].jpg'),
(1107, 'prd_images/[1107][0].jpg'),
(1108, 'prd_images/[1108][0].jpg'),
(1109, 'prd_images/[1109][0].jpg'),
(1110, 'prd_images/[1110][0].jpg'),
(1111, 'prd_images/[1111][0].jpg'),
(1112, 'prd_images/[1112][0].jpg'),
(1113, 'prd_images/[1113][0].jpg'),
(1114, 'prd_images/[1114][0].jpg'),
(1115, 'prd_images/[1115][0].jpg'),
(1116, 'prd_images/[1116][0].jpg'),
(1117, 'prd_images/[1117][0].jpg'),
(1118, 'prd_images/[1118][0].jpg'),
(1119, 'prd_images/[1119][0].jpg'),
(1120, 'prd_images/[1120][0].jpg'),
(1121, 'prd_images/[1121][0].jpg'),
(1122, 'prd_images/[1122][0].png'),
(1123, 'prd_images/[1123][0].png'),
(1124, 'prd_images/[1124][0].jpg'),
(1126, 'prd_images/[1126][0].png'),
(1127, 'prd_images/[1127][0].jpg'),
(1128, 'prd_images/[1128][0].jpg'),
(1129, 'prd_images/[1129][0].png'),
(1130, 'prd_images/[1130][0].jpg'),
(1131, 'prd_images/[1131][0].jpg'),
(1132, 'prd_images/[1132][0].jpg'),
(1133, 'prd_images/[1133][0].jpg'),
(1134, 'prd_images/[1134][0].jpg'),
(1135, 'prd_images/[1135][0].jpg'),
(1137, 'prd_images/[1137][0].jpg'),
(1136, 'prd_images/[1136][1].jpg'),
(1138, 'prd_images/[1138][0].jpg'),
(1139, 'prd_images/[1139][0].jpg'),
(1140, 'prd_images/[1140][0].jpg'),
(1141, 'prd_images/[1141][0].jpg'),
(1142, 'prd_images/[1142][0].jpg'),
(1143, 'prd_images/[1143][0].jpg'),
(1144, 'prd_images/[1144][0].JPG'),
(1145, 'prd_images/[1145][0].jpg'),
(1146, 'prd_images/[1146][0].webp'),
(1147, 'prd_images/[1147][0].jpg'),
(1148, 'prd_images/[1148][0].jpg'),
(1149, 'prd_images/[1149][0].jpg'),
(1150, 'prd_images/[1150][0].jpg'),
(1151, 'prd_images/[1151][0].jpg'),
(1152, 'prd_images/[1152][0].jpg'),
(1153, 'prd_images/[1153][0].jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_ID` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_ID`, `username`, `password`) VALUES
(101, 'Admin', 'password');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `log_complain`
--
ALTER TABLE `log_complain`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `log_notif`
--
ALTER TABLE `log_notif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_price_update`
--
ALTER TABLE `log_price_update`
  ADD KEY `Log_Price_Update_fk0` (`prd_ID`);

--
-- Indexes for table `numbers`
--
ALTER TABLE `numbers`
  ADD PRIMARY KEY (`number`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD KEY `Product_Images_fk0` (`prd_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_ID`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=526;

--
-- AUTO_INCREMENT for table `log_complain`
--
ALTER TABLE `log_complain`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10013;

--
-- AUTO_INCREMENT for table `log_notif`
--
ALTER TABLE `log_notif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1154;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
