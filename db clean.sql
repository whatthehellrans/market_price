CREATE TABLE `Category` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`Cat_Name` varchar(256) NOT NULL,
	`Primary_Category` enum('Basic Necessities','Prime Commodities') DEFAULT 'Basic Necessities' NOT NULL,
	PRIMARY KEY (`ID`)
)
AUTO_INCREMENT=500;


CREATE TABLE `Products` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`prd_Name` varchar(255) NOT NULL,
	`prd_Local` varchar(255),
	`prd_Details` varchar(5000),
	`prd_Category` INT NOT NULL,
	`prd_Price` DECIMAL NOT NULL,
	`prd_Unit` varchar(255) NOT NULL,
	PRIMARY KEY (`ID`)
)
AUTO_INCREMENT=1000;


CREATE TABLE `Product_Images` (
	`prd_ID` INT NOT NULL,
	`img_Loc` varchar(5000) NOT NULL
);

CREATE TABLE `Log_Price_Update` (
	`log_Date` TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
	`prd_ID` INT NOT NULL,
	`old_price` double(20,2) NOT NULL,
	`new_price` double(20,2) NOT NULL
);

CREATE TABLE `log_complain` (
	`ID` int(11) NOT NULL AUTO_INCREMENT,
	`log_Date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`name` varchar(255) NOT NULL DEFAULT 'Unknown',
	`contact` varchar(255) NOT NULL DEFAULT '09123456789',
	`address` varchar(255) NOT NULL DEFAULT 'Daraga',
	`category` varchar(255) NOT NULL DEFAULT 'category 1',
	`shop` varchar(255) NOT NULL,
	`message` varchar(20000) NOT NULL,
	PRIMARY KEY (`ID`)
) AUTO_INCREMENT=10000;

CREATE TABLE `log_notif`(
	`id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT
)AUTO_INCREMENT=1;


CREATE TABLE `numbers` (
	`number` varchar(13) PRIMARY KEY
);

CREATE TABLE `Users` (
	`user_ID` INT NOT NULL AUTO_INCREMENT,
	`username` varchar(255) NOT NULL UNIQUE,
	`password` varchar(255) NOT NULL,
	PRIMARY KEY (`user_ID`)
)
AUTO_INCREMENT=100;

CREATE TABLE `keywords`(
 `prd_ID` int,
 `keyword` varchar(1000)
);

ALTER TABLE `keywords` ADD CONSTRAINT `keywords_fk0` FOREIGN KEY (`prd_ID`) REFERENCES `Products`(`ID`);

ALTER TABLE `Category` ADD CONSTRAINT `Category_fk0` FOREIGN KEY (`ID`) REFERENCES `Products`(`prd_Category`);

ALTER TABLE `Product_Images` ADD CONSTRAINT `Product_Images_fk0` FOREIGN KEY (`prd_ID`) REFERENCES `Products`(`ID`);

ALTER TABLE `Log_Price_Update` ADD CONSTRAINT `Log_Price_Update_fk0` FOREIGN KEY (`prd_ID`) REFERENCES `Products`(`ID`);

ALTER TABLE `Log_Actions` ADD CONSTRAINT `Log_Actions_fk0` FOREIGN KEY (`user_ID`) REFERENCES `Users`(`user_ID`);
