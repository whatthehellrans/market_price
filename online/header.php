<?php
date_default_timezone_set('Asia/Manila');
$currentFileName = basename($_SERVER["SCRIPT_FILENAME"], '.php')

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Daraga Market Price Monitoring System</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat+Alternates">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
</head>

<body>
    <div class="bg-primary">
        <nav class="navbar container">
            <div class="text-white mt-2">
                <h2>Daraga Public Market Price Monitoring System</h2>
            </div>
        </nav>
        <div id="head">
            <div class="jumbotron text-left">
                <!-- <div class="container">
                    <div class="row" style="border:1px solid">
                        <div class="col">
                            <img class="market-logo img-fluid" src="assets/img/daraga-logo.png" alt="">
                            <img class="market-logo img-fluid" src="assets/img/daraga-market-logo.png" alt="">
                        </div>
                        
                        <div class="col">
                            <h4>Republic of the Philippines <br>
                                                Province of Albay <br>
                                                MUNICIPALITY OF DARAGA</h4>
                        </div>
                        <div class="col">
                            <p style="text-align:right;">Philippine Standard Time: <br> Sunday, October 27, 2019 8:13PM  </p>
                        </div>
                    </div>
                </div> -->


            
                <div class="flexcontainer">
                    <img class="market-logo img-fluid" src="assets/img/daraga-logo.png" alt="">
                    <img class="market-logo img-fluid" src="assets/img/daraga-market-logo.png" alt="">
                    <p style="font-size:18px; font-family: 'Times New Roman', Times, serif;">Republic of the Philippines <br>
                                            Province of Albay <br>
                                            MUNICIPALITY OF DARAGA</p>
                    <p style="text-align:right;">Philippine Standard Time: <br> <?php echo date("l F d, Y - g:iA");?></p>
                </div>
           
            </div>
        </div>

        <nav class="navbar navbar-light navbar-expand-md">
            <div class="container">
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link <?php if($currentFileName == "index") {echo "active";} ?>" href="index.php">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link <?php if($currentFileName == "Category" && $_GET["cat"] == 1) {echo "active";} ?>" href="Category.php?cat=1">Basic Necessities</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link <?php if($currentFileName == "Category" && $_GET["cat"] == 2) {echo "active";} ?>" href="Category.php?cat=2">Prime Commodities</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link <?php if($currentFileName == "Complain") {echo "active";} ?>" href="Complain.php">Complain</a></li>
                        <!-- <li class="nav-item" role="presentation"><a class="nav-link <?php if($currentFileName == "Keyword") {echo "active";} ?>" href="Keywords.php">Keyword Dictionary</a></li> -->
                    </ul>
                </div>
                
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <!-- Search form -->
                            <form action="Result.php" method="get">
                                <div class="active-cyan-3 active-cyan-4">
                                    <input type="search" class="form-control"  name="search" placeholder="Search">
                                </div>
                            </form>
                        </li>
                    </ul>
                
            </div>
        </nav>

    </div>

    