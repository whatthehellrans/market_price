<?php
    include_once("header.php");
    include_once("admin/php/functions.php");
    
    $errors = ["shop" => "", "message" => "","name"=>"","contact"=>"","address"=>"","category"=>""];
    $successMsg = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){

        $shopname = cleanInput($_POST["shop"]);
	$category = cleanInput($_POST["category"]);
        $message  = cleanInput($_POST["message"]);
        $name     = cleanInput($_POST["name"]);
        $contact  = cleanInput($_POST["contact"]);
        $address  = cleanInput($_POST["address"]);

        //check for error
        $errors["shop"]	     = checkEmpty($shopname, "Shop name cannot be Empty.");
	$errors["category"]	     = checkEmpty($shopname, "Category name cannot be Empty.");
        $errors["message"]	 = checkEmpty($message, "Message cannot be Empty.");
        $errors["name"]	     = checkEmpty($name, "Name cannot be Empty.");
        $errors["contact"]	 = checkEmpty($contact, "Contact cannot be Empty.");
        $errors["address"]	     = checkEmpty($address, "Address cannot be Empty.");
        
        $digits = strlen($contact);
        if($digits != 10){
            $errors["contact"] = "Contact number must be valid (10 Digit). Ex. 9123456789";
        }

        $noError = true;
        foreach ($errors as $er) {
            if($er != ""){
                $noError = false;
            }
        }

        if($noError){
            $contact = "0".$contact;
            //Proceed to success and insert
            include_once("admin/php/Control/LogsController.php");
            $logsController = new LogsController();
            if($logsController->InsertComplainMsg($name,$contact,$address,$category,$shopname,$message)){
                $successMsg = "Message has been successfuly sent.";
            }
        }
    }

?>
    <!-- Start: Projects Clean -->
    <div class="projects-clean">
        <div class="container">
            <!-- Start: Intro -->
            <div class="intro">
                <h2 class="text-center">Send a Complaint Message</h2>
            </div>
            <!-- End: Intro -->
            <?php 
                showSuccessMsg($successMsg);
             ?>
            <form action="" method="post">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="name">Sender Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                            <?php showErrorMsg($errors["name"]); ?>
                        </div>

                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contact">Contact </label>
                            <input type="number" class="form-control" id="contact" name="contact">
                            <?php showErrorMsg($errors["contact"]); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="address">Address </label>
                            <input type="text" class="form-control" id="address" name="address">
                            <?php showErrorMsg($errors["address"]); ?>
                        </div>
                    </div>
		    <div class="col-sm-3">
                        <label for="market-category">Market Category</label>
                    	<select class="form-control" name="category" id="market-category" onchange="ChangeSelection()" >
							<!-- <option value="Category 0"> </option> -->
                        	<option value="Fish Section">Fish Section</option>
                        	<option value="Dried Fish Section">Dried Fish Section</option>
                        	<option value="Meat Section">Meat Section</option>
							<option value="Grocery Store Section">Grocery Store Section</option>
							<option value="Fruits Section">Fruits Section</option>
                    	</select>
			<?php showErrorMsg($errors["category"]); ?>
                    </div>
			
                </div>
                <div class="form-group">
                    <label for="market-shop">Shop Involved</label>
                    <select class="form-control" name="shop" id="market-shop" >
                        <option value="LCC"></option>
                        <option value="Bhert & Lhet Meat Shop">Bhert & Lhet Meat Shop</option>
                        <option value="Mañago's Meat Shop">Mañago's Meat Shop</option>
                        <option value="Dan & Amy Verdillo Meat Shop">Dan & Amy Verdillo Meat Shop</option>
                        <option value="A. Boseo Meat Shop">A. Boseo Meat Shop</option>
                        <option value="Robas Meat Shop">Robas Meat Shop</option>
                        <option value="Wen's Meat Shop">Wen's Meat Shop</option>
						<option value="Betty Nuñez Meat Shop">Betty Nuñez Meat Shop</option>
					    <option value="C-S Meat Shop">C-S Meat Shop</option>
						<option value="DM Marcellana Meat Shop">DM Marcellana Meat Shop</option>
						<option value="Ding Galang's Meat Shop">Ding Galang's Meat Shop</option>
						<option value="Catz Galang's Meat shop">Catz Galang's Meat shop</option>
						<option value="Napay Meat Shop">Napay Meat Shop</option>
						<option value="E.Llaneta Meat Shop">E.Llaneta Meat Shop</option>
						<option value="Boyet Masbate Meat Shop">Boyet Masbate Meat Shop</option>
						<option value="Peter Neo Fish Section">Peter Neo Fish Section</option>
						<option value="Elsa Lobos Fish Section">Elsa Lobos Fish Section</option>
						<option value="Alex Luna Fish Section">Alex Luna Fish Section</option>
						<option value="Ma's Meat Shop">Ma's Meat Shop</option>
						<option value="B.M. Benasa's Meat Shop">B.M. Benasa's Meat Shop</option>
						<option value="Llaneta Store">Llaneta Store</option>
						<option value="Heaven Forest Meat Shop">Heaven Forest Meat Shop</option>
						<option value="3D's Meat Shop">3D's Meat Shop</option>
						<option value="Big Boy Store">Big Boy Store</option>
						<option value="Odec-Wholesaler">Odec-Wholesaler</option>
						<option value="Boboy Store">Boboy Store</option>
						<option value="Ed Santos Store">Ed Santos Store</option>
                        
                    </select>
                    <?php showErrorMsg($errors["shop"]); ?>
                </div>
                <?php 
                    showErrorMsg($errors["message"]);
                ?>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" id="message" name="message" rows="7"></textarea>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary w-50">Submit</button>
                </div>
                
            </form>
        </div>
    </div>
    <!-- End: Projects Clean -->
    <!-- TO DO -- add page
    <div class="resultspages">
        <p class="text-center">Showing Page 1 of 3</p><a class="pagesnumber" href="#">1</a><a class="pagesnumber" href="#">2</a>
        <a class="pagesnumber" href="#">3</a>
    </div> -->

    <script src="assets/js/complain.js"></script>
        <?php 
	include_once("footer.php");
?>