<?php
    include_once("header.php");
	include_once("php/functions.php");
	include_once("php/Control/ProductController.php");
	include_once("php/Control/CategoryController.php");

	$error = $success = "";


	$categoryController	= new CategoryController();
	$productController	= new ProductController();
    $Categories			= $categoryController->FetchAllCategory();


?>

                <div class="container-fluid">
					<div class="d-sm-flex justify-content-between align-items-center mb-4">
					<h3 class="text-dark mb-0">Choose a Product to modify</h3>
					</div>
					<a class="btn btn-primary mb-4" href="../admin/Add-Category.php">Add New Category</a>
					<div class="row">
						<?php foreach ($Categories as $category) { 
							$products = $productController->FetchProductBaseOnCategory($category->getID());
							if(count($products) == 0){
								//-- Skip Category if there is no Product on it.
								continue;
							}

							?>
							<div class="col-md-6 col-xl-5">
								<h4 > <a href="Edit-Product.php?category=<?php echo $category->getID()?>" class="text-info"> <?php echo $category->getCat_Name(); ?></a>    </h4>
								
							</div>

						<?php
						}
						?>
						
					</div>
                </div>
            </div>


<?php
include_once("footer.php");
?>