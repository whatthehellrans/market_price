<?php
    session_start();
    include_once("php/functions.php");
    checkIfLogin();

    session_destroy();
    header("location: login.php?logout=1");

?>