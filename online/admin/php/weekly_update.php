<?php 
    date_default_timezone_set('Asia/Manila');
    // include_once("Control\SMSController.php");

    // $smsController = new SMSController();
    
    // if($smsController->InsertNumber("1234567890")){
    //     $add = "Successfully added.";
    // }else{
    //     $error = 'Failed to add.(Number already Exists)';
    // }

    include_once("Control\LogsController.php");
    include_once("Control\SMSController.php");
    include_once("Control\ProductController.php");

    $productController = new ProductController();
    $logsController = new LogsController();
    $smsController = new SMSController();

    $logs = $logsController->fetchWeekLog();

    $msg = "Daraga Market Price Weekly Update: ";
    if(count($logs) != 0){
        foreach ($logs as $log) {
            $date = date_create(substr($log["log_Date"],0,10));
            $date = date_format($date,"l"); 
            $product_name = $productController->FetchSingleProduct($log["id"])->getPrd_Name();
            $new_price = $log["price"];
            $msg .= "
			Day: $date 
			Item: $product_name 
			New Price: P$new_price ";
        }
    }else{
        $msg .= "No Price Update for this week.";
    }

    $smsController->sendSms($msg);
    

?>