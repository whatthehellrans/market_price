<?php

include_once(dirname(__FILE__)."/../Model/Product.php");

class ProductController
{
    public $connection;

    public function __construct()
    {
        include_once("Config.php");
        $this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        // Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
    }
    
    public function __destruct()
    {
        $this->connection->close();
    }

    public function checkDuplicate($toSearch)
    {
        $sql	= "SELECT `prd_Name` FROM `Products` WHERE `prd_Name` LIKE '$toSearch'";
        $result	= $this->connection->query($sql);
        if ($result->num_rows > 0) {
            return "Product name already Exists.<br>";
        }
    }

    public function InsertNewProduct($prd)
    {
        $prd_Name		= mysqli_real_escape_string($this->connection,$prd->getPrd_Name());
        $prd_Local		= mysqli_real_escape_string($this->connection,$prd->getPrd_Local());
		$prd_Details	= mysqli_real_escape_string($this->connection,$prd->getPrd_Details());
		$prd_Category	= mysqli_real_escape_string($this->connection,$prd->getPrd_Category());
		$prd_Price		= mysqli_real_escape_string($this->connection,$prd->getPrd_Price());
		$prd_Unit		= mysqli_real_escape_string($this->connection,$prd->getPrd_Unit());

        $sql    = "INSERT INTO `products` (`ID`, `prd_Name`,`prd_Local`,`prd_Details`,`prd_Category`,`prd_Price`,`prd_Unit`)
                   VALUES (NULL,'$prd_Name','$prd_Local','$prd_Details','$prd_Category','$prd_Price','$prd_Unit' )";
        if ($this->connection->query($sql) === true) {
            return true;
        } else {
            return false;
        }
    }
    
    public function FetchLastEntry()
    {
        $sql = "SELECT `ID` FROM `products` ORDER BY ID DESC LIMIT 1";
        $result = $this->connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                return $row["ID"];
            }
        }
    }

    public function FetchSearchProduct($name){
        $search			= mysqli_real_escape_string($this->connection,$name);
        $sql = "SELECT * FROM `products` WHERE `prd_Name` LIKE '%$search%'";
        $result = $this->connection->query($sql);
        $productArray = array();
        if($result->num_rows > 0){
            
            while($row = $result->fetch_assoc()){
                $prd = new Product( $row["ID"],$row["prd_Name"],$row["prd_Local"],$row["prd_Details"],
                                    $row["prd_Category"],$row["prd_Price"],$row["prd_Unit"]);
				array_push($productArray,$prd);
            }
            
        }
        return $productArray;
    }

    public function FetchRandomProduct($size){
        $sql = "SELECT * FROM `products` ORDER BY RAND() LIMIT $size";
        $result = $this->connection->query($sql);

        if($result->num_rows > 0){
            $productArray = array();
            while($row = $result->fetch_assoc()){
                $prd = new Product( $row["ID"],$row["prd_Name"],$row["prd_Local"],$row["prd_Details"],
                                    $row["prd_Category"],$row["prd_Price"],$row["prd_Unit"]);
				array_push($productArray,$prd);
            }
            return $productArray;
        }
    }

    public function FetchSingleRandomProduct($category){
        $sql = "SELECT * FROM `products` WHERE `prd_Category` LIKE '$category' ORDER BY RAND() LIMIT 1";
        $result = $this->connection->query($sql);

        if($result->num_rows > 0){;
            while($row = $result->fetch_assoc()){
                $prd = new Product( $row["ID"],$row["prd_Name"],$row["prd_Local"],$row["prd_Details"],
                                    $row["prd_Category"],$row["prd_Price"],$row["prd_Unit"]);
				return $prd;
            }
        }
    }

    public function FetchProductBaseOnCategory($category){
		$sql = "SELECT * FROM `products` WHERE `prd_Category` LIKE '$category'";
		$result = $this->connection->query($sql);
        $prdArr = array();
		if($result->num_rows > 0){
			
			while($row = $result->fetch_assoc()){
				$prd = new Product( $row["ID"],$row["prd_Name"],$row["prd_Local"],$row["prd_Details"],
									$row["prd_Category"],$row["prd_Price"],$row["prd_Unit"]);
				array_push($prdArr,$prd);	
			}
			
        }
        return $prdArr;
    }
    public function FetchRelatedProduct($category,$exclude,$size){
		$sql = "SELECT * FROM `products` WHERE `prd_Category` LIKE '$category' AND `ID` NOT LIKE '$exclude' ORDER BY RAND() LIMIT $size";
		$result = $this->connection->query($sql);

		if($result->num_rows > 0){
			$prdArr = array();
			while($row = $result->fetch_assoc()){
				$prd = new Product( $row["ID"],$row["prd_Name"],$row["prd_Local"],$row["prd_Details"],
									$row["prd_Category"],$row["prd_Price"],$row["prd_Unit"]);
				array_push($prdArr,$prd);	
			}
			return $prdArr;
		}
    }
    
	
	public function FetchSingleProduct($id){
		$sql = "SELECT * FROM `products` WHERE `ID` LIKE '$id'";
		$result = $this->connection->query($sql);

		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				return new Product( $row["ID"],$row["prd_Name"],$row["prd_Local"],$row["prd_Details"],
				$row["prd_Category"],$row["prd_Price"],$row["prd_Unit"]);
			}
		}
    }
    
    public function DeleteProduct($id){
        $prdID		= mysqli_real_escape_string($this->connection,$id);
	
        $sql        = " DELETE FROM `products` WHERE `ID` = $prdID";
        $sql2       = " DELETE FROM `keywords` WHERE `prd_ID` = $prdID";

        if ($this->connection->query($sql) === TRUE && $this->connection->query($sql2) === TRUE) {
			return TRUE;
		} else {
			echo "Error: " . $sql . "<br>" . $this->connection->error;
			return FALSE;
		}
    }

    public function UpdateProduct($prd)
    {
        $prd_ID			= mysqli_real_escape_string($this->connection,$prd->getID());
        $prd_Name		= mysqli_real_escape_string($this->connection,$prd->getPrd_Name());
        $prd_Local		= mysqli_real_escape_string($this->connection,$prd->getPrd_Local());
		$prd_Details	= mysqli_real_escape_string($this->connection,$prd->getPrd_Details());
		$prd_Category	= mysqli_real_escape_string($this->connection,$prd->getPrd_Category());
		$prd_Price		= mysqli_real_escape_string($this->connection,$prd->getPrd_Price());
		$prd_Unit		= mysqli_real_escape_string($this->connection,$prd->getPrd_Unit());

		$sql	= " UPDATE `products`
					SET `prd_Name`		= '$prd_Name',
						`prd_Local`		= '$prd_Local',
						`prd_Details`	= '$prd_Details',
						`prd_Category`	= '$prd_Category',
						`prd_Price`		= '$prd_Price',
						`prd_Unit`		= '$prd_Unit'
					WHERE `ID` LIKE '$prd_ID'";
        if ($this->connection->query($sql) === true) {
            return true;
        } else {
            return false;
        }
    }
}
