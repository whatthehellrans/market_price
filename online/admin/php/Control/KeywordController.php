<?php

include_once(dirname(__FILE__)."/../Model/Tag.php");

class KeywordController
{

	var $connection;

	function __construct()
	{
		include_once("Config.php");
		$this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		// Check connection
		if ($this->connection->connect_error) {
			die("Connection failed: " . $this->connection->connect_error);
		}
	}

	function __destruct(){
		$this->connection->close();
	}


	function InsertPrdKeyword($prdID,$key){
		$keyword =  mysqli_real_escape_string($this->connection,$key);

		$sql = "INSERT INTO `keywords` (`prd_ID`, `keyword`) VALUES ($prdID, '$keyword')";
		if($this->connection->query($sql) === TRUE){
			return TRUE;
		}else{
			echo $sql . "<br>";
			echo $this->connection->error;
			return FALSE;
		}
	}

	function FetchPrdKeyword($prdID){
		$keywords = array();
		$sql = "SELECT `keyword` FROM `keywords` WHERE `prd_ID` LIKE $prdID";
		$result = $this->connection->query($sql);
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				array_push($keywords,$row["keyword"]);
			}
		}
		return $keywords;
	}

	function FetchSearchKeyword($name){
		$search			= mysqli_real_escape_string($this->connection,$name);
        $sql = "SELECT prd_ID FROM `keywords` WHERE `keyword` LIKE '%$search%'";
        $result = $this->connection->query($sql);
        $productArray = array();
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
				array_push($productArray,$row["prd_ID"]);
            }
            
        }
        return $productArray;
	}

	function DeletePrdKeyword($prdID){
		$sql	= " DELETE FROM `keywords`
					WHERE `prd_ID` LIKE $prdID";
		if ($this->connection->query($sql) === TRUE) {
			return TRUE;
		} else {
			echo "Error: " . $sql . "<br>" . $this->connection->error;
			return FALSE;
		}
	}





}
