<?php


class LogsController
{

	var $connection;

	function __construct()
	{
		include_once("Config.php");
		$this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		// Check connection
		if ($this->connection->connect_error) {
			die("Connection failed: " . $connection->connect_error);
		}
	}

	function __destruct(){
		$this->connection->close();
	}



	function InsertPriceUpdate($prdID,$oldPrice,$newPrice){
		$sql	= " INSERT INTO `log_price_update`(`prd_ID`,`old_price`,`new_price`)
					VALUES ($prdID,$oldPrice,$newPrice)";
		if($this->connection->query($sql) === TRUE){
			return TRUE;
		}else{
			echo "Error: " . $sql . "<br>" . $this->connection->error;
			return FALSE;
		}
	}

	function fetchPriceUpdatesOfProduct($prd,$size){
		$sql	= "SELECT * FROM `log_price_update` WHERE `prd_ID` LIKE '$prd' ORDER BY `log_Date` DESC LIMIT $size";
		$result = $this->connection->query($sql);
		$logsArr = array();
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
				$log = array("log_Date" => $row["log_Date"],
							 "prd_ID"	=> $row["prd_ID"],
							 "old_price"=> $row["old_price"],
							 "new_price"=> $row["new_price"]);
				array_push($logsArr,$log);
			}
		}
		return $logsArr;
	}

	function fetchWeekLog(){
		//echo date("Y-m-d");
		$previous_week = strtotime("-1 week");
		$previous_week = date("Y-m-d",$previous_week);
		$current_date = strtotime("+1 day");
		$current_date = date("Y-m-d",$current_date);

		$sql = "SELECT * from `log_price_update` WHERE log_Date BETWEEN '$previous_week%' and '$current_date%'  ORDER BY log_Date DESC";
		$result = $this->connection->query($sql);
		
		$allLogs = array();
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$item = array("log_Date" => $row["log_Date"],
							  "id"       => $row["prd_ID"],
							  "price" 	 => $row["new_price"]);
				
				$proceed = TRUE;
				foreach ($allLogs as $i) {
					if($i["id"] == $item["id"]){
						$proceed = FALSE;
						break;
					}
				}

				if($proceed){
					array_push($allLogs,$item);
				}
			}
		}
		return $allLogs;
	}

	function fetchLatestPriceUpdate($size){
		$sql	= "SELECT * FROM `log_price_update` ORDER BY `log_Date` DESC LIMIT $size";
		$result = $this->connection->query($sql);
		$logsArr = array();
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
				$log = array("log_Date" => $row["log_Date"],
							 "prd_ID"	=> $row["prd_ID"],
							 "old_price"=> $row["old_price"],
							 "new_price"=> $row["new_price"]);
				array_push($logsArr,$log);
			}
		}
		return $logsArr;
	}

	



	/***************
	** Complaint Message
	**
	***************/

	function InsertComplainMsg($name,$contact,$address,$category,$shop,$message){
		$data_name			= mysqli_real_escape_string($this->connection,$name);
		$data_contact		= mysqli_real_escape_string($this->connection,$contact);
		$data_address		= mysqli_real_escape_string($this->connection,$address);
		$data_shop			= mysqli_real_escape_string($this->connection,$shop);
		$data_message		= mysqli_real_escape_string($this->connection,$message);
		$data_category		= mysqli_real_escape_string($this->connection,$category);			
		
		
		$sql	= " INSERT INTO `log_complain`(`name`,`contact`,`category`,`shop`,`message`,`address`)
					VALUES ('$data_name','$data_contact', '$data_category','$data_shop','$data_message', '$data_address')";
		if($this->connection->query($sql) === TRUE){
			$this->InsertComplainNotif();
			return TRUE;
		}else{
			echo "Error: " . $sql . "<br>" . $this->connection->error;
			return FALSE;
		}
	}

	function InsertComplainNotif(){
		$sql = "INSERT INTO `log_notif` VALUES (null)";		
		if($this->connection->query($sql) === TRUE){
			return TRUE;
		}else{
			echo "Error: " . $sql . "<br>" . $this->connection->error;
			return FALSE;
		}
	}
	function ClearComplainNotif(){
		$sql = "TRUNCATE `log_notif`";
		if($this->connection->query($sql) === TRUE){
			return TRUE;
		}else{
			echo "Error: " . $sql . "<br>" . $this->connection->error;
			return FALSE;
		}
	}

	function fetchLogNotif(){
		$sql = "SELECT * FROM `log_notif`";
		$result = $this->connection->query($sql);
		if($result->num_rows > 0){
			return true;
		}else{
			return false;
		}
	}

	function fetchLatestComplain($size){
		$sql	= "SELECT * FROM `log_complain` ORDER BY `log_Date` DESC LIMIT $size";
		$result = $this->connection->query($sql);
		$logsArr = array();
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
				$log = array("ID" 		=> $row["ID"],
							 "log_Date" => $row["log_Date"],
							 "name"	    => $row["name"],
							 "contact"  => $row["contact"],
							 "address"  => $row["address"],
							"category" => $row["category"],
							 "shop"	    => $row["shop"],
							 "message"  => $row["message"]);
				array_push($logsArr,$log);
			}
		}
		return $logsArr;
	}

	function fetchComplaintMessage($id){
		$sql	= "SELECT * FROM `log_complain` WHERE `ID` LIKE $id;";
		$result = $this->connection->query($sql);
		if($result->num_rows > 0){
			while($row =$result->fetch_assoc()){
				return array(	"ID" 		=> $row["ID"],
								"log_Date" => $row["log_Date"],
								"name"	   => $row["name"],
								"contact"  => $row["contact"],
								"address"  => $row["address"],
								"category" => $row["category"],
								"shop"	    => $row["shop"],
								"message"  => $row["message"]);
			}
		}
		return false;
	}

	function fetchComplaintMsgDate($date,$bymonth){
		if($bymonth){
			$date = substr($date,0,7);
		}

		$sql	= "SELECT * FROM `log_complain` WHERE `log_Date` LIKE '%$date%' ORDER BY `log_Date` DESC";
		$result = $this->connection->query($sql);
		$logsArr = array();
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
				$log = array("ID" 		=> $row["ID"],
							 "log_Date" => $row["log_Date"],
							 "name" 	=> $row["name"],
							 "contact"  => $row["contact"],
							 "address"  => $row["address"],
							"category" => $row["category"],
							 "shop"	    => $row["shop"],
							 "message"  => $row["message"]);
				array_push($logsArr,$log);
			}
		}
		return $logsArr;
	}
	 

}
