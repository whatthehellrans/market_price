<?php

include_once(dirname(__FILE__)."/../Model/Category.php");

class CategoryController
{

	var $connection;

	function __construct()
	{
		include_once("Config.php");
		$this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		// Check connection
		if ($this->connection->connect_error) {
			die("Connection failed: " . $connection->connect_error);
		}
    }
    
    function __destruct(){
		$this->connection->close();
	}

	function checkDuplicate($toSearch)
	{
		$sql	= "SELECT `cat_Name` FROM `Category` WHERE `cat_Name` LIKE '$toSearch'";
		$result	= $this->connection->query($sql);
		if ($result->num_rows > 0) {
			return "Category already Exists.<br>";
		}
	}

	function InsertNewCategory($CategoryClass){
		$cat_Name		=  mysqli_real_escape_string($this->connection,$CategoryClass->getCat_Name());
		$cat_Primary	=  mysqli_real_escape_string($this->connection,$CategoryClass->getPrimary_Category());
		
		$sql	= "INSERT INTO `Category` (`ID`, `cat_Name`,`primary_Category`)
					VALUES (NULL, '$cat_Name','$cat_Primary')";
		if($this->connection->query($sql) === TRUE){
			return TRUE;
		}else{
			return FALSE;
		}
    }
    
    function FetchAllCategory(){
		$CategoryArr = array();
		$sql	= "SELECT * FROM `category`";
		$result = $this->connection->query($sql);
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$cat = new Category($row["ID"],$row["cat_Name"],$row["primary_Category"]);
				array_push($CategoryArr,$cat);
			}
		}
		return $CategoryArr;
	}

	function FetchSearchCategory($name){
		$search			= mysqli_real_escape_string($this->connection,$name);
        $sql = "SELECT `ID` FROM `category` WHERE `cat_Name` LIKE '%$search%'";
        $result = $this->connection->query($sql);
        $categoryArray = array();
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
				array_push($categoryArray,$row["ID"]);
            }
            
        }
        return $categoryArray;
	}

	function FetchSingleCategory($id){
		$sql	= "SELECT * FROM `category` WHERE `ID` LIKE '$id'";
		$result	= $this->connection->query($sql);
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				return new Category($row["ID"],$row["cat_Name"],$row["primary_Category"]);
			}
		}
	}

	function UpdateCategory($Category){
		$cat_Name		= mysqli_real_escape_string($this->connection,$Category->getCat_Name());
		$cat_Primary	= mysqli_real_escape_string($this->connection,$Category->getPrimary_Category());
		$sql	=	"UPDATE `category`
					 SET `cat_Name` = '$cat_Name',
					 	 `primary_Category` = '$cat_Primary' 
					 WHERE `category`.`ID` = " . $Category->getID();
		if ($this->connection->query($sql) === TRUE) {
			return TRUE;
		} else {
			echo "Error: " . $sql . "<br>" . $this->connection->error;
			return FALSE;
		}
	}

	function DeleteCategory($CategoryID){
		$catID		= mysqli_real_escape_string($this->connection,$CategoryID);
	
		$sql		= " DELETE FROM `category`
						WHERE ID = $catID";
		if ($this->connection->query($sql) === TRUE) {
			return TRUE;
		} else {
			echo "Error: " . $sql . "<br>" . $this->connection->error;
			return FALSE;
		}
	}
}
