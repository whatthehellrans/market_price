<?php

//include_once(dirname(__FILE__)."/../Model/Image.php");

class ImageController
{

	var $connection;

	function __construct()
	{
		include_once("Config.php");
		$this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		// Check connection
		if ($this->connection->connect_error) {
			die("Connection failed: " . $connection->connect_error);
		}
	}

	function __destruct(){
		$this->connection->close();
	}



	function InsertNewImage($prdID,$imgPath){
		$sql	= "INSERT INTO `product_images` (`prd_ID`, `img_Loc`) VALUES ($prdID, '$imgPath')";
		if($this->connection->query($sql) === TRUE){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function fetchSingleImage($id){
		$sql	= "SELECT `img_Loc` FROM `product_images` WHERE `prd_ID` LIKE $id LIMIT 1";
		$result = $this->connection->query($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                return $row["img_Loc"];
            }
		}
	}

	function fetchImagesOfProduct($id){
		$sql	= "SELECT `img_Loc` FROM `product_images` WHERE `prd_ID` LIKE $id";
		$result = $this->connection->query($sql);
		$imgArr = array();
        if($result->num_rows > 0){
			
            while($row = $result->fetch_assoc()){
				array_push($imgArr,$row["img_Loc"]);
			}
			
		}
		return $imgArr;
	}

	function deleteImageOfProduct($imgLoc){
		$loc		= mysqli_real_escape_string($this->connection,$imgLoc);
		$sql		= " DELETE FROM `product_images`
						WHERE `img_Loc` = '$loc'";
        if ($this->connection->query($sql) === TRUE) {
			//return TRUE;
		} else {
			echo "Error: " . $sql . "<br>" . $this->connection->error."<br>";
			//return FALSE;
		}
	}

	

}
