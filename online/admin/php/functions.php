<?php

    function checkIfLogin(){
        if(!isset($_SESSION['User']))
        {
            header("location: login.php");
            exit();
        }
    }

    function checkUser(){
        if(isset($_SERVER["username"])){
            return TRUE;
        }
        return FALSE;
    }

    //Call in Login Page only
    function checkIfAlreadyLogin(){
        if(isset($_SESSION['User']))
        {
            header("location: index.php");
            exit();
        }
    }

    function returnSelected($data,$data2){
        if($data === $data2){
            echo 'selected=""';
        }
    }

    function returnChecked($data,$data2){
        if($data === $data2){
            echo 'checked=""';
        }
    }

    function cleanInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    function checkEmpty($data, $msg)
    {
        if (empty($data)) {
            return $msg . "<br>";
        }
    }

    
    //Changes the color to green if new price is lower, red if higher
    function priceUpdateColor($old,$new){
        if($old > $new){
            return "text-success";
        }
            return "text-danger";
    }
    
    function showErrorMsg($emsg)
    {
        if ($emsg != "") { ?>
			<small class="form-text text-danger"><?php echo $emsg; ?></small> <?php
        }
    }
    function showSuccessMsg($emsg)
    {
        if ($emsg != "") { ?>
			<small class="form-text text-success"><?php echo $emsg; ?></small> <?php
        }
    }
    
?>