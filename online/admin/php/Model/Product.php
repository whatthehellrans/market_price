<?php

class Product
{
    private $ID;
    private $prd_Name;
    private $prd_Local;
    private $prd_Details;
    private $prd_Category;
    private $prd_Price;
    private $prd_Unit;
    
    public function __construct($ID, $prd_Name, $prd_Local, $prd_Details, $prd_Category, $prd_Price, $prd_Unit)
    {
        $this->ID = $ID;
        $this->prd_Name     = $prd_Name;
        $this->prd_Local    = $prd_Local;
        $this->prd_Details	= $prd_Details;
        $this->prd_Category = $prd_Category;
        $this->prd_Price	= $prd_Price;
        $this->prd_Unit		= $prd_Unit;
    }


    public function getID()
    {
        return $this->ID;
    }

    public function setID($ID)
    {
        $this->ID = $ID;
        return $this;
    }

    public function getPrd_Name()
    {
        return $this->prd_Name;
    }

    public function setPrd_Name($prd_Name)
    {
        $this->prd_Name = $prd_Name;
        return $this;
    }

    public function getPrd_Local()
    {
        return $this->prd_Local;
    }

    public function setPrd_Local($prd_Local)
    {
        $this->prd_Local = $prd_Local;
        return $this;
    }

    public function getPrd_Details()
    {
        return $this->prd_Details;
    }

    public function setPrd_Details($prd_Details)
    {
        $this->prd_Details = $prd_Details;
        return $this;
    }

    public function getPrd_Category()
    {
        return $this->prd_Category;
    }

    public function setPrd_Category($prd_Category)
    {
        $this->prd_Category = $prd_Category;
        return $this;
    }

    public function getPrd_Price()
    {
        return $this->prd_Price;
    }

    public function setPrd_Price($prd_Price)
    {
        $this->prd_Price = $prd_Price;
        return $this;
    }

    public function getPrd_Unit()
    {
        return $this->prd_Unit;
    }

    public function setPrd_Unit($prd_Unit)
    {
        $this->prd_Unit = $prd_Unit;
        return $this;
    }
}
