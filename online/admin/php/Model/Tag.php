<?php

class Tag
{
    private $ID;
    private $Tag_Name;
    
    function __construct($ID,$Tag_Name){
        $this->ID = $ID;
        $this->Tag_Name = $Tag_Name;
    }

    public function getID()
    {
        return $this->ID;
    }

    public function setID($ID)
    {
        $this->ID = $ID;
        return $this;
    }

    public function getTag_Name()
    {
        return $this->Tag_Name;
    }

    public function setTag_Name($Tag_Name)
    {
        $this->Tag_Name = $Tag_Name;
        return $this;
    }
}
