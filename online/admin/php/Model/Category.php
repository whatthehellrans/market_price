<?php

class Category
{
    private $ID;
    private $cat_Name;
    private $primary_Category;
    
    public function __construct($ID, $cat_Name, $primary_Category)
    {
        $this->ID = $ID;
        $this->cat_Name = $cat_Name;
        $this->primary_Category = $primary_Category;
    }


    public function getID()
    {
        return $this->ID;
    }

    public function setID($ID)
    {
        $this->ID = $ID;
        return $this;
    }

    public function getCat_Name()
    {
        return $this->cat_Name;
    }

    public function setCat_Name($cat_Name)
    {
        $this->cat_Name = $cat_Name;
        return $this;
    }

    public function getPrimary_Category()
    {
        return $this->primary_Category;
    }

    public function setPrimary_Category($primary_Category)
    {
        $this->primary_Category = $primary_Category;
        return $this;
    }
}
