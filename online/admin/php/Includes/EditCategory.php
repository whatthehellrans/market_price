<?php
    include_once("header.php");
    include_once("php/functions.php");
    
    $error = array("Category_Name"=>"", "Primary_Category"=>"");
    $success = "";

	$Category = $categoryController->FetchSingleCategory($_GET["CatID"]);
	
	if(isset($_GET["DeleteConfirm"]) || !empty($_GET["DeleteConfirm"])){
        if($_GET["DeleteConfirm"] == 1){
            if($categoryController->DeleteCategory($Category->getID()) ){
				$msg = "Category has been Deleted.";
				include_once(dirname(__DIR__)."/Control/ProductController.php");
				include_once(dirname(__DIR__)."/Control/ImageController.php");
				$productController	= new ProductController();
				$imageController	= new ImageController();
				$productsOfCategory = $productController->FetchProductBaseOnCategory($Category->getID());
				echo "Total Product Inside Category: ".count($productsOfCategory) ."<br>";
				foreach ($productsOfCategory as $prd) {
					//show product and product name
					$prdImages = $imageController->fetchImagesOfProduct($prd->getID());
					foreach($prdImages as $img){
						$imageController->deleteImageOfProduct($img);
						unlink($img);
					}
					$productController->DeleteProduct($prd->getID());
				}

            }else{
                $msg = "Deletion Failed";
            }
            ?>
    
            <script type="text/javascript">
            window.alert("<?php echo $msg;?>")
            </script>
            <script> location.replace("Edit-Category.php"); </script>
    
            <?php
            exit();
        }
    
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $safe = "";

        $NewCategory = new Category($Category->getID(), cleanInput($_POST["Category_Name"]), cleanInput($_POST["Primary_Class"]));
        

        $error["Category_Name"] = checkEmpty($NewCategory->getCat_Name(), "Category Name cannot be Empty.");
		
		//Do we still need to check for duplicate name??
		//$error["Category_Name"] .= $categoryController->checkDuplicate($NewCategory->getCat_Name());

        $error["Primary_Category"] = checkEmpty($NewCategory->getPrimary_Category(), "Class cannot be empty");
        if ($NewCategory->getPrimary_Category() === "Basic Necessities" || $NewCategory->getPrimary_Category() === "Prime Commodities") {
            //do Nothing
        }else{
            $error["Primary_Category"] .= "Invalid Class";
        }
        
        foreach ($error as $errorIndividual) {
            if ($errorIndividual != "") {
                $safe = "Has Error";
            }
        }

        if ($safe == "") {
            if ($categoryController->UpdateCategory($NewCategory)) {
				$success = "Category has been Modified.";
				//TODO -- ADD LOG
            } 
		}
		$Category = $NewCategory;

    }
?>
<div class="container-fluid">
    <div class="d-sm-flex justify-content-between align-items-center mb-4">
        <h3 class="text-dark mb-0">Editing Category <span class="text-info"><?php echo $Category->getCat_Name();?></span></h3>
    </div>
    <div class="row">
        <div class="col">
            <form class="formCatTag" method="post">
                <?php
                            showErrorMsg($error["Category_Name"]);
                            showSuccessMsg($success);
                ?>
                <div class="form-group"><input class="form-control" type="text" name="Category_Name"
                        placeholder="Category Name" autofocus="" required="" value="<?php echo $Category->getCat_Name();?>"></div>
                <?php
                            showErrorMsg($error["Primary_Category"]);
                ?>
                <div class="form-group"><select class="custom-select" name="Primary_Class" required="">
                        <optgroup label="Class">
                            <option value="Basic Necessities" <?php returnSelected($Category->getPrimary_Category(),"Basic Necessities");?> >Basic Necessities</option>
                            <option value="Prime Commodities" <?php returnSelected($Category->getPrimary_Category(),"Prime Commodities");?>>Prime Commodities</option>
                        </optgroup>
                    </select></div>
                <div class="form-group"><button class="btn btn-primary btn-lg w-100" type="submit">send </button></div>
            </form>
        </div>
    </div>
</div>
</div>
<footer class="bg-white sticky-footer">
    <div class="container my-auto">
        <div class="text-center my-auto copyright"><span>Daraga Market Price Monitoring System 2019</span></div>
    </div>
</footer>
</div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
<script src="assets/js/script.min.js?h=9af5f655239f5ce4fa692ca1c1513d50"></script>
</body>

</html>