            <?php  $products = $productController->FetchProductBaseOnCategory($category->getID()); ?>
            <div class="d-sm-flex justify-content-between align-items-center mb-4">
                <h3 class="text-dark mb-0">Choose a Product to modify</h3>
            </div>
            <a class="btn btn-primary mb-4" href="../admin/Add-Product.php">Add New Product</a>
            <div class="row">
                    <div class="col-md-6 col-xl-5">
                        <h4><span class="text-info"> <?php echo $category->getCat_Name(); ?> </span> </h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($products as $prd) { ?>
                                        <tr>
                                            <th scope="row"><?php echo $prd->getID();?></th>
                                            <td><?php echo $prd->getPrd_Name();?></td>
                                            <td><a href="Edit-Product.php?PrdID=<?php echo $prd->getID();?>">Edit</a> | <a
                                                    href="Edit-Product.php?PrdID=<?php echo $prd->getID();?>&DeleteConfirm=1"
                                                    onclick="return confirm('Deleting this Category will also Delete the Product Belonging to it.\nAre you sure you want to Delete this Category?')">Delete</a>
                                            </td>
                                        </tr>
                                <?php
                                    }
                                ?>

                            </tbody>
                        </table>
                    </div>

               
                


            </div>