<?php
    include_once("header.php");
    include_once("php/functions.php");
    include_once("php/Control/KeywordController.php");
    include_once("php/Control/ImageController.php");
    include_once("php/Control/LogsController.php");
    
    $logController = new LogsController();			
    $keywordController = new KeywordController();
    $imgController = new ImageController();

    $Product = $productController->FetchSingleProduct($_GET["PrdID"]);
    $CategoryArr = $categoryController->FetchAllCategory();


    $error = array("Product_Name"=>"","Category"=>"","Unit"=>"","Price"=>"","Image"=>"");
   
    $success = $safe = "";
    
    //for Product Deletion
    if(!empty($_GET["DeleteConfirm"]) || isset($_GET["DeleteConfirm"])){
        if($_GET["DeleteConfirm"] == 1){
            if( $productController->DeleteProduct($Product->getID())){
                $prdImages = $imgController->fetchImagesOfProduct($Product->getID());
                foreach ($prdImages as $imgKey) {
                    $imgController->deleteImageOfProduct($imgKey);
                    unlink($imgKey);
                }   
            }else{
                $msg = "Deletion Failed";
            }
            ?>
    
            <script type="text/javascript">
            window.alert("<?php echo $msg;?>")
            </script>
            <script> location.replace("Edit-Product.php"); </script>
    
            <?php
            exit();
        }
    }


    //For Image Deletion
    if(!empty($_GET["DelImage"]) || isset($_GET["DelImage"])){
        $imgToDelete = $_GET["DelImage"];
        $imgController->deleteImageOfProduct($imgToDelete);
        unlink($imgToDelete);
        ?>
        <script type="text/javascript">
            window.alert("Image Has Been Deleted.");
        </script>
        <script> location.replace("Edit-Product.php?PrdID=<?php echo $Product->getID();?>"); </script>
        <?php
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $newProduct = new Product(
            $Product->getID(),
            cleanInput($_POST["Product_Name"]),
            cleanInput($_POST["Local_Name"]),
            cleanInput($_POST["Details"]),
            cleanInput($_POST["Category"]),
            cleanInput($_POST["Price"]),
            cleanInput($_POST["Unit"])
        );
        
        //Check Product Name if Empty + Duplicate Entry
        //Check Category if Empty.
        $error["Product_Name"]	 = checkEmpty($newProduct->getPrd_Name(), "Product name cannot be Empty.");
        //$error["Product_Name"]	.= $ProductController->checkDuplicate($Product->getPrd_Name()); --> Duplicates allowed
        $error["Category"]		.= checkEmpty($newProduct->getPrd_Category(), "Category cannot be Empty.");

        //Check if Category is Valid
        $CategoryExist = false;
        foreach ($CategoryArr as $cat) {
            if ($cat->getID() == $newProduct->getPrd_Category()) {
                $CategoryExist = true;
                break;
            }
        }
        if (!$CategoryExist) {
            $error["Category"]	.= "Invalid Category.<br>";
        }
        
        //Check Unit + Price if empty.
        $error["Unit"]	.= checkEmpty($newProduct->getPrd_Unit(), "Unit cannot be Empty.");
        $error["Price"]	.= checkEmpty($newProduct->getPrd_Price(), "Price cannot be Empty.");
        
        //Check Price if Non-Numeric
        if (preg_match("/^[a-zA-Z ]*$/", $newProduct->getPrd_Price())) {
            $error["Price"] .= "Please Use Numbers Only.<br>";
        }
        
        //Check for Image Upload
        /* --Not Required
        if (!is_uploaded_file($_FILES["images"]["tmp_name"][0])) {
            $error["Image"] .= "Please Select an Image to Upload.<br>";
        }
        */

 
        foreach ($error as $errorIndividual) {
            if ($errorIndividual != "") {
                $safe = "Has Error";
            }
        }
        
       
        
        if ($safe == "") {
            if ($productController->UpdateProduct($newProduct)) {
                //Logs Price Update (if it change)
                if($Product->getPrd_Price() != $newProduct->getPrd_Price()){
                    //Insert Price Logs Update - (product ID, Old Price, New Price)
                    $logController->InsertPriceUpdate($newProduct->getID(),$Product->getPrd_Price(),$newProduct->getPrd_Price());
                    //Price Update SMS NOTIF
                    if(SEND_SMS){
                        include_once("php/Control/SMSController.php");
                        $smsController = new SMSController();
                        $message = "Daraga Market Price Update: ".$newProduct->getPrd_Name()." - New Price: P". $newProduct->getPrd_Price();
                        $smsController->sendSms($message);

                    }
                }

				//Update Tags (Deleting then Re-Adding)
                $keywordController->DeletePrdKeyword($Product->getID());
                //Keyword Handling-------------------
                if(isset($_POST["keywords"]) && !empty($_POST["keywords"])){
                    $keywords = cleanInput($_POST["keywords"]);
                    //Split String into Array by Commas
                    $keywords = explode(",",$keywords);
                    foreach ($keywords as $keyword) {
                        //Check if element is empty before passing it
                        if($keyword != "" || $keyword == " "){
                            $keywordController->InsertPrdKeyword($Product->getID(),cleanInput($keyword));
                        }
                    }
                }
                

				//Image Handling------------------
				// $files = array_filter($_FILES['upload']['name']); //something like that to be used before processing files.
				// Count # of uploaded files in array
				$total = count($_FILES['images']['name']);
                $totalCurrentImage = count($imgController->fetchImagesOfProduct($Product->getID()));
				// Loop through each file
				for ($i= $totalCurrentImage; $i < $totalCurrentImage + $total ; $i++) {
					//Get the temp file path
					$tmpFilePath = $_FILES['images']['tmp_name'][$i - $totalCurrentImage];
					//Make sure we have a file path
					if ($tmpFilePath != "") {
						//Handle Renaming of File
						$temp = explode(".", $_FILES["images"]["name"][$i - $totalCurrentImage]);
						$newfilename = "[". $Product->getID() ."][".$i."]." . end($temp);
						//Setup new file path
						$newFilePath = "prd_images/" . $newfilename;
						//Upload the file into the temp dir
						if (move_uploaded_file($tmpFilePath, $newFilePath)) {
							//Insert Image to DB
							$imgController->InsertNewImage($Product->getID(),$newFilePath);
						}
					}
				}

                $success = "Product has been modified.";
                echo $newProduct->getPrd_Category();
                header("Location: Edit-Product.php?category=". $newProduct->getPrd_Category());
                exit();
            } else {
                $error["Category_Name"] .= "Error: " . $sql . "<br>" . $this->connection->error;
            }



        }
        
        $Product = $newProduct;
    }


    $Keywords = $keywordController->FetchPrdKeyword($Product->getID());
    sort($Keywords);
    if(!empty($Keywords)){
        $Keywords = implode(", ",$Keywords);
    }else{
        $Keywords = "";
    }
?>
        
                    <div class="d-sm-flex justify-content-between align-items-center mb-4">
                        <h3 class="text-dark mb-0">Editing <span class="text-info"><?php echo $Product->getPrd_Name();?></span></h3>
                    </div>
                    <?php 
                        showSuccessMsg($success);
                    ?>
                    <div class="row">
                        <div class="col">
                            <form method="post" enctype="multipart/form-data">
                                <div class="form-group"><input class="form-control" type="text" name="Product_Name"
                                        placeholder="Product Name" autofocus="" required="" value="<?php echo $Product->getPrd_Name();?>"></div>
                                <div class="form-group"><input class="form-control" type="text" name="Local_Name"
                                        placeholder="Local Name (Optional)"  value="<?php echo $Product->getPrd_Local();?>"></div>
                                <div class="form-group"><input class="form-control" type="text" name="Details"
                                        placeholder="Details"  value="<?php echo $Product->getPrd_Details();?>"></div>
                                <div class="form-group"><small class="form-text">Category</small><select
                                        class="form-control" name="Category" required="">
                                        <optgroup label="Category">
											<?php
                                                    foreach ($CategoryArr as $cat) { ?>
														<option <?php returnSelected($Product->getPrd_Category(),$cat->getID());?> value="<?php echo $cat->getID(); ?>"><?php echo $cat->getCat_Name();?></option>
											<?php	}
                                            ?>
                                        </optgroup>
                                    </select></div>
                                <div class="form-group"><input class="form-control" type="text" name="Unit"
                                        placeholder="Unit ( eg. 330mL)" required="" value="<?php echo $Product->getPrd_Unit();?>"></div>
                                <div class="form-group"><input class="form-control" type="number" step="0.01" name="Price"
                                        placeholder="Price per Unit" value="<?php echo $Product->getPrd_Price();?>" ></div>
                                <small>Separate by Commas ","</small>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="keywords" placeholder="Keyword" value="<?php echo $Keywords;?>">
                                </div>
                                <div class="form-group text-center"><button class="btn btn-primary btn-lg w-75"
                                        type="submit">Submit</button></div>
                          
                        </div>
                        <div class="col">
							<h1>Add Image</h1>
							<input type="file" multiple="multiple" name="images[]" accept="image/*">
                        </div>
                        </div>
					</form>
                </div>
            </div>
            
            <div class="row ml-5 mr-5">
                <?php 
                $imagesArr = $imgController->fetchImagesOfProduct($Product->getID());
                foreach ($imagesArr as $imgKey) { ?>
                    <div class="col-md-6 col-xl-3 mb-4">
                        <div class="card shadow boarder-left-primary py-2 text-center" >
                            <img src="<?php echo $imgKey;?>" alt="" class="img-fluid " style="height:12em;" >
                            <a href="Edit-Product.php?PrdID=<?php echo $Product->getID();?>&DelImage=<?php echo $imgKey;?>"
                               class="btn btn-primary dashbtn text-white 
                               <?php if(count($imagesArr) == 1) echo "disabled"?>">Delete</a>
                        </div>
                    </div>   
                <?php
                }
                ?>

            </div>

			
<?php
    include_once("footer.php");

?>