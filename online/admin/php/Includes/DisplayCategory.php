            <div class="d-sm-flex justify-content-between align-items-center mb-4">
                <h3 class="text-dark mb-0">Choose Category to modify</h3>
            </div>
            <a class="btn btn-primary mb-4" href="../admin/Add-Category.php">Add New Category</a>
            <div class="row">
                <div class="col">
                    <h4>Basic Necessities</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Category ID</th>
                                <th scope="col">Category Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            foreach ($Categories as $cat) {
                                if($cat->getPrimary_Category() === "Prime Commodities"){
                                    continue;
                                } ?>
                            <tr>
                                <th scope="row"><?php echo $cat->getID();?></th>
                                <td><?php echo $cat->getCat_Name();?></td>
                                <td><a href="Edit-Category.php?CatID=<?php echo $cat->getID();?>">Edit</a> | <a
                                        href="Edit-Category.php?CatID=<?php echo $cat->getID();?>&DeleteConfirm=1"
                                        onclick="return confirm('Deleting this Category will also Delete the Product Belonging to it.\nAre you sure you want to Delete this Category?')">Delete</a>
                                </td>

                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

                <div class="col">
                    <h4>Prime Commodities</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Category ID</th>
                                <th scope="col">Category Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($Categories as $cat) { 
                                if($cat->getPrimary_Category() === "Basic Necessities"){
                                    continue;
                                }?>
                            <tr>
                                <th scope="row"><?php echo $cat->getID();?></th>
                                <td><?php echo $cat->getCat_Name();?></td>
                                <td><a href="Edit-Category.php?CatID=<?php echo $cat->getID();?>">Edit</a> | <a
                                        href="Edit-Category.php?CatID=<?php echo $cat->getID();?>&DeleteConfirm=1"
                                        onclick="return confirm('Deleting this Category will also Delete the Product Belonging to it.\nAre you sure you want to Delete this Category?')">Delete</a>
                                </td>

                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>