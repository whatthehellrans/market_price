<?php
    include_once("header.php");
    include_once("php/functions.php");
    include_once("php/Control/LogsController.php");
    include_once("php/Control/ProductController.php");
    $logController = new LogsController();	
    $productController = new ProductController();
    
    $latestSize = 5; //How many items should we display?

    $priceUpdate    = $logController->fetchLatestPriceUpdate($latestSize);

?>


<div class="container-fluid">
    <div class="d-sm-flex justify-content-between align-items-center mb-4">
        <h3 class="text-dark mb-0">Daraga Market Price Monitoring System Admin Panel</h3>
    </div>
 
    <div class="row">
        <div class="col">
            <p>Lastest Price Update</p>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Old Price</th>
                            <th>New Price</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        

                        foreach ($priceUpdate as $logs) {
                            
                            ?>

                            <tr>
                                <td><?php echo $logs["prd_ID"];?> - <span class="text-info"><?php echo $productController->fetchSingleProduct($logs["prd_ID"])->getPrd_Name();?></span></td>
                                <td>P<?php echo $logs["old_price"];?></td>
                                <td class="<?php echo priceUpdateColor($logs["old_price"],$logs["new_price"]);?>" >P<?php echo $logs["new_price"];?></td>
                                <td><?php echo $logs["log_Date"];?></td>
                            </tr>

                        <?php
                        }
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php 
    include_once("footer.php");

?>