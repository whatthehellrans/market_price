

<?php
    include_once("header.php");
    include_once("php/Control/ProductController.php");
    include_once("php/Control/ImageController.php");
    include_once("php/Control/CategoryController.php");
    
    include_once("php/Model/Category.php");
    
	$categoryController	= new CategoryController();
	$producController	= new ProductController();
	$imageController	= new ImageController();

	$categoryArr		= $categoryController->FetchAllCategory();

	// Divide into 2 Table if product of category exceed 7.
	$countSlice			= 7;
	
?>

<!-- Start: Projects Clean -->
<div class="projects-clean">
    <div class="container">
		<!-- Start: Intro -->


		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<?php
				
				for($i = 0;$i < count($categoryArr); $i++){
					//Do not Display Category if it has no Product in it.
					$productList = $producController->FetchProductBaseOnCategory($categoryArr[$i]->getID());
					if(count($productList) == 0){
						continue;
					}
					?>
					<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i;?>" <?php if($i == 0) {echo 'class="active"';}?>></li>
				<?php
				}
				?>
			</ol>
			<div class="carousel-inner" role="listbox" style="width:100%; min-height: 763px !important;">
				<?php

				for($i = 0;$i < count($categoryArr); $i++){
					//Do not Display Category if it has no Product in it.
					$productList = $producController->FetchProductBaseOnCategory($categoryArr[$i]->getID());
					if(count($productList) == 0){
						continue;
					}

					$divideIntoTwo = FALSE;
					if(count($productList) > $countSlice){
						$divideIntoTwo = TRUE;
					}
					//Divide Products into 2
					$slicePrdArr = array_chunk($productList,ceil(count($productList)/2)); 
					?>
					<div class="carousel-item  <?php if($i == 0) echo "active";?>">
						<div class="intro">
							<h3 class="text-center mt-5"><strong><?php echo $categoryArr[$i]->getCat_Name();?></strong></h3>
							<p class="text-center"><?php echo $categoryArr[$i]->getPrimary_Category();?></p>
						</div>
						<?php 

							if($divideIntoTwo){?>
								<div class="row">
									<div class="col">
										<table class="table">
											<thead>
												<tr>
												<th scope="col">Name</th>
												<th scope="col">Price</th>
												<th scope="col">Unit</th>
												</tr>
											</thead>
											<tbody>
												<?php 
													foreach ($slicePrdArr[0] as $prd) { ?>
														<tr>
															<th scope="row"><?php echo $prd->getPrd_Name();?></th>
															<td>₱<?php echo $prd->getPrd_Price();?></td>
															<td><?php echo $prd->getPrd_Unit();?></td>
														</tr>
													<?php
													}
												?>
											</tbody>
										</table>
									</div>

									<div class="col">
										<table class="table">
											<thead>
												<tr>
												<th scope="col">Name</th>
												<th scope="col">Price</th>
												<th scope="col">Unit</th>
												</tr>
											</thead>
											<tbody>
												<?php 
													foreach ($slicePrdArr[1] as $prd) { ?>
														<tr>
															<th scope="row"><?php echo $prd->getPrd_Name();?></th>
															<td>₱<?php echo $prd->getPrd_Price();?></td>
															<td><?php echo $prd->getPrd_Unit();?></td>
														</tr>
													<?php
													}
												?>
											</tbody>
										</table>
									</div>
								</div>

							<?php
							}else{ ?>
								<table class="table">
									<thead>
										<tr>
										<th scope="col">Name</th>
										<th scope="col">Price</th>
										<th scope="col">Unit</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach ($productList as $prd) { ?>
												<tr>
													<th scope="row"><?php echo $prd->getPrd_Name();?></th>
													<td>₱<?php echo $prd->getPrd_Price();?></td>
													<td><?php echo $prd->getPrd_Unit();?></td>
												</tr>
											<?php
											}
										?>
									</tbody>
								</table>

							<?php
							}
						?>
						

						
						

					</div>
				<?php
                }
				?>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<div class="text-center m-3">
			<button id="togglebtn" class="btn btn-primary " onclick="openFullscreen();">Fullscreen</button>
		</div>
		

        <!-- End: Intro -->
      
    </div>
</div>

<script>
	/* Get the element you want displayed in fullscreen */ 
	let elem = document.documentElement;

	/* Function to open fullscreen mode */
	let fullscreen = false;
	function openFullscreen() {

		if(!fullscreen)
		{
			if (elem.requestFullscreen) {
				elem.requestFullscreen();
			} else if (elem.mozRequestFullScreen) { /* Firefox */
				elem.mozRequestFullScreen();
			} else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
				elem.webkitRequestFullscreen();
			} else if (elem.msRequestFullscreen) { /* IE/Edge */
				elem = window.top.document.body; //To break out of frame in IE
				elem.msRequestFullscreen();
			}
		}else{
			if (document.exitFullscreen) {
				document.exitFullscreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
			} else if (document.msExitFullscreen) {
				window.top.document.msExitFullscreen();
			}
		}
		//elem.style.visibility = "hidden";

		document.getElementById("togglebtn").style.visibility = "hidden";
		fullscreen = !fullscreen;
	}


	/* document.addEventListener("fullscreenchange", function() {
		console.log("test");
		
	}); */

	// Events
	
	document.addEventListener("fullscreenchange", function() {
		hideAdminNav();
	});
	document.addEventListener("mozfullscreenchange", function() {
		hideAdminNav();
	});
	document.addEventListener("webkitfullscreenchange", function() {
		ideAdminNav();
	});
	document.addEventListener("msfullscreenchange", function() {
		ideAdminNav();
	});

	function hideAdminNav()
	{
		let admin_nav = document.getElementById("admin-navigation");
		if( window.innerHeight == screen.height) {
			admin_nav.style.display = "none";
		}else{
			admin_nav.style.display = "block";
		}
	}
</script>
<!-- End: Projects Clean -->

<?php 
	include_once("footer.php");
?>