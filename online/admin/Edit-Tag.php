<?php
    include_once("header.php");
	include_once("php/functions.php");
	include_once("php/Control/TagController.php");

	$tagController	= new TagController();
    $error = $success = "";
    $displayAll		= FALSE;
    
	
	if(!isset($_GET["TagID"]) || empty($_GET["TagID"])){
		$Tags			= $tagController->FetchAllTag();
		$displayAll = TRUE;
	}

?>

                <div class="container-fluid">
					<?php 
						if($displayAll){
							include_once("php/Includes/DisplayTags.php");
						}else{
							include_once("php/Includes/EditTag.php");
						}
					?>
                </div>
            </div>


<?php
include_once("footer.php");
?>