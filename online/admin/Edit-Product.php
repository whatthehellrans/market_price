<?php
    include_once("header.php");
	include_once("php/functions.php");
	include_once("php/Control/ProductController.php");
	include_once("php/Control/CategoryController.php");

	$error = $success = "";
	$displayAll		= FALSE;

	$categoryController	= new CategoryController();
    $productController	= new ProductController();
    
	if(!isset($_GET["PrdID"]) || empty($_GET["PrdID"])){
		$category			= $categoryController->FetchSingleCategory($_GET["category"]);
		$displayAll = TRUE;
	}



?>

                <div class="container-fluid">
					<?php 
						
						if($displayAll){
							include_once("php/Includes/DisplayProducts.php");
						}else{
							include_once("php/Includes/EditProduct.php");
						}
					?>
                </div>
            </div>


<?php
include_once("footer.php");
?>