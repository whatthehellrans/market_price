<?php
    include_once("header.php");
    include_once("php/functions.php");
    include_once("php/Control/CategoryController.php");

    $CategoryController = new CategoryController();

    $CategoryArr = $CategoryController->FetchAllCategory();
    
    
    $error = array("Product_Name"=>"","Category"=>"","Unit"=>"","Price"=>"","Image"=>"");
   
	$success = $safe = "";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        
 
        include_once("php/Control/ProductController.php");
        include_once("php/Control/KeywordController.php");
        
        $KeywordController = new KeywordController();
        $ProductController = new ProductController;
        $Product = new Product(
            0,
            cleanInput($_POST["Product_Name"]),
            cleanInput($_POST["Local_Name"]),
            cleanInput($_POST["Details"]),
            cleanInput($_POST["Category"]),
            cleanInput($_POST["Price"]),
            cleanInput($_POST["Unit"])
        );
        
        //Check Product Name if Empty + Duplicate Entry
        //Check Category if Empty.
        $error["Product_Name"]	 = checkEmpty($Product->getPrd_Name(), "Product name cannot be Empty.");
        //$error["Product_Name"]	.= $ProductController->checkDuplicate($Product->getPrd_Name()); --> Duplicates allowed
        $error["Category"]		.= checkEmpty($Product->getPrd_Category(), "Category cannot be Empty.");

        //Check if Category is Valid
        $CategoryExist = false;
        foreach ($CategoryArr as $cat) {
            if ($cat->getID() == $Product->getPrd_Category()) {
                $CategoryExist = true;
                break;
            }
        }
        if (!$CategoryExist) {
            $error["Category"]	.= "Invalid Category.<br>";
        }
        
        //Check Unit + Price if empty.
        $error["Unit"]	.= checkEmpty($Product->getPrd_Unit(), "Unit cannot be Empty.");
        $error["Price"]	.= checkEmpty($Product->getPrd_Price(), "Price cannot be Empty.");
        
        //Check Price if Non-Numeric
        if (preg_match("/^[a-zA-Z ]*$/", $Product->getPrd_Price())) {
            $error["Price"] .= "Please Use Numbers Only in Price.<br>";
        }
        
        //Check for Image Upload
        if (!is_uploaded_file($_FILES["images"]["tmp_name"][0])) {
            $error["Image"] .= "Please Select an Image to Upload.<br>";
        }

    
        
        foreach ($error as $errorIndividual) {
            if ($errorIndividual != "") {
                $safe = "Has Error";
            }
        }
        
       


        
        if ($safe == "") {
            if ($ProductController->InsertNewProduct($Product)) {
				//Fetch Last ID
				$lastID = $ProductController->FetchLastEntry();

                //Keyword Handling-------------------
                if(isset($_POST["keywords"]) && !empty($_POST["keywords"])){
                    $keywords = cleanInput($_POST["keywords"]);
                    //Split String into Array by Commas
                    $keywords = explode(",",$keywords);
                    foreach ($keywords as $keyword) {
                        //Check if element is empty before passing it
                        if($keyword != "" || $keyword == " "){
                            $KeywordController->InsertPrdKeyword($lastID,cleanInput($keyword));
                        }
                    }
                }


				//Image Handling------------------
				include_once("php/Control/ImageController.php");
				$ImgController = new ImageController();

				// $files = array_filter($_FILES['upload']['name']); //something like that to be used before processing files.
				// Count # of uploaded files in array
				$total = count($_FILES['images']['name']);
				
				// Loop through each file
				for ($i=0 ; $i < $total ; $i++) {
					//Get the temp file path
					$tmpFilePath = $_FILES['images']['tmp_name'][$i];
					//Make sure we have a file path
					if ($tmpFilePath != "") {
						//Handle Renaming of File
						$temp = explode(".", $_FILES["images"]["name"][$i]);
						$newfilename = "[".$lastID ."][".$i."]." . end($temp);
						//Setup new file path
						$newFilePath = "prd_images/" . $newfilename;
						//Upload the file into the temp dir
						if (move_uploaded_file($tmpFilePath, $newFilePath)) {
							//Insert Image to DB
							$ImgController->InsertNewImage($lastID,$newFilePath);
						}
					}
				}

				$success = "Product has been added.";

            } else {
                $error["Category_Name"] .= "Error: " . $sql . "<br>" . $this->connection->error;
            }



        }
        
        //Destroying Objects
        $Product = null;
        $ProductController = null;
    }
?>
                <div class="container-fluid">
                    <div class="d-sm-flex justify-content-between align-items-center mb-4">
                        <h3 class="text-dark mb-0">Add an Item</h3>
                    </div>
                    <?php 
                        showSuccessMsg($success);
                        foreach ($error as $e) {
                            showErrorMsg($e);
                        }
                    ?>
                    <div class="row">
                        <div class="col">
                            <form method="post" enctype="multipart/form-data">
                                <div class="form-group"><input class="form-control" type="text" name="Product_Name"
                                        placeholder="Product Name" autofocus="" required=""></div>
                                <div class="form-group"><input class="form-control" type="text" name="Local_Name"
                                        placeholder="Local Name (Optional)"></div>
                                <div class="form-group"><input class="form-control" type="text" name="Details"
                                        placeholder="Details"></div>
                                <div class="form-group"><small class="form-text">Category</small><select
                                        class="form-control" name="Category" required="">
                                        <optgroup label="Category">
											<?php
                                                    foreach ($CategoryArr as $cat) { ?>
														<option value="<?php echo $cat->getID(); ?>"><?php echo $cat->getCat_Name();?></option>
											<?php	}
                                            ?>
                                        </optgroup>
                                    </select></div>
                                <div class="form-group"><input class="form-control" type="text" name="Unit"
                                        placeholder="Unit ( eg. 330mL)" required=""></div>
                                <div class="form-group"><input class="form-control" type="number" step="0.01" name="Price"
                                        placeholder="Price per Unit">
                                </div>
                                <small>Separate by Commas ","</small>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="keywords" placeholder="Keyword">
                                </div>
                                <div class="form-group text-center"><button class="btn btn-primary btn-lg w-75"
                                        type="submit">Submit</button></div>
                                
                        </div>
                        <div class="col ">
                         
							<h1>Upload Image</h1>
							<input type="file" multiple="multiple" name="images[]" accept="image/*" required="">
						</div>
					</div>
					</form>
                </div>
			</div>
			
<?php
    include_once("footer.php");

?>