<?php 
    session_start();
    include_once("php/functions.php");
    checkIfLogin();


    //Check for Log complain
    include_once("php/Control/LogsController.php");
    $LogsController = new LogsController();
    $hasNewNotif = $LogsController->fetchLogNotif();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Daraga Market Price Monitoring System Admin Panel</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css?h=4d1e091e564b66954c885008dc1a7a9b">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
</head>

<body id="page-top">
    <div id="wrapper" >
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-dark p-0" id="admin-navigation">
            <div class="container-fluid d-flex flex-column p-0" >
                <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="index.php">
                    <div class="sidebar-brand-text mx-3"><span>Admin Panel<br></span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="nav navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="index.php"><i class="fas fa-tachometer-alt"></i><span>Dashboard</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link " href="Products-Category.php"><i class="fas fa-tags"></i><span>Products</span></a></li>
    
                    <li class="nav-item" role="presentation"><a class="nav-link" href="Edit-Category.php"><i class="fas fa-shopping-cart"></i><span>Category</span></a></li>

                    <li class="nav-item" role="presentation"><a class="<?php if($hasNewNotif) echo "text-info";?>  nav-link" href="Complain.php"><i class="fas fa-thumbs-down"></i><span>Complaint</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="Smsnotif.php"><i class="fas fa-sms"></i><span>SMS Notification</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="pricelist.php"><i class="fas fa-surprise"></i><span>Price List</span></a></li>
                    
                    
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper" >
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="nav navbar-nav flex-nowrap ml-auto">
                            
                            
                            <div class="d-none d-sm-block topbar-divider"></div>
                            <li class="nav-item dropdown no-arrow" role="presentation">
                                <li class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><span class=" d-lg-inline mr-2 text-gray-600 small"><?php echo $_SESSION["User"];?></span></a>
                                    <div class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu">
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" role="presentation" href="logout.php"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </li>
                            </li>
                        </ul>
                    </div>
                </nav>