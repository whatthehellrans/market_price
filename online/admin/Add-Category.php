<?php
    include_once("header.php");
    include_once("php/functions.php");
    
    $error = array("Category_Name"=>"", "Primary_Category"=>"");
    $success = "";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $safe = "";

        include_once("php/Control/CategoryController.php");
        include_once("php/Model/Category.php");


        $CategoryController = new CategoryController;
        $Category = new Category(0, cleanInput($_POST["Category_Name"]), cleanInput($_POST["Primary_Class"]));
        

        $error["Category_Name"] = checkEmpty($Category->getCat_Name(), "Category Name cannot be Empty.");
        $error["Category_Name"] .= $CategoryController->checkDuplicate($Category->getCat_Name());

        $error["Primary_Category"] = checkEmpty($Category->getPrimary_Category(), "Class cannot be empty");
        if ($Category->getPrimary_Category() === "Basic Necessities" || $Category->getPrimary_Category() === "Prime Commodities") {
            //do Nothing
        }else{
            $error["Primary_Category"] .= "Invalid Class";
        }
        
        foreach ($error as $errorIndividual) {
            if ($errorIndividual != "") {
                $safe = "Has Error";
            }
        }

        if ($safe == "") {
            if ($CategoryController->InsertNewCategory($Category)) {
                $success = "Category has been added.";
            } else {
                $error["Category_Name"] .= "Error: " . $sql . "<br>" . $this->connection->error;
            }
        }
        //Destroying Objects
        $Category = null;
        $CategoryController = null;
    }
?>
<div class="container-fluid">
    <div class="d-sm-flex justify-content-between align-items-center mb-4">
        <h3 class="text-dark mb-0">New Category</h3>
    </div>
    <div class="row">
        <div class="col">
            <form class="formCatTag" method="post">
                <?php
                            showErrorMsg($error["Category_Name"]);
                            showSuccessMsg($success);
                ?>
                <div class="form-group"><input class="form-control" type="text" name="Category_Name"
                        placeholder="Category Name" autofocus="" required=""></div>
                <?php
                            showErrorMsg($error["Primary_Category"]);
                ?>
                <div class="form-group"><select class="custom-select" name="Primary_Class" required="">
                        <optgroup label="Class">
                            <option value="Basic Necessities" selected="">Basic Necessities</option>
                            <option value="Prime Commodities">Prime Commodities</option>
                        </optgroup>
                    </select></div>
                <div class="form-group"><button class="btn btn-primary btn-lg w-100" type="submit">send </button></div>
            </form>
        </div>
    </div>
</div>
</div>
<footer class="bg-white sticky-footer">
    <div class="container my-auto">
        <div class="text-center my-auto copyright"><span>Daraga Market Price Monitoring System 2019</span></div>
    </div>
</footer>
</div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
<script src="assets/js/script.min.js?h=9af5f655239f5ce4fa692ca1c1513d50"></script>
</body>

</html>