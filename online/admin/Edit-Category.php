<?php
    include_once("header.php");
	include_once("php/functions.php");
	include_once("php/Control/CategoryController.php");

	$categoryController	= new CategoryController();
    $error = $success = "";
    $displayAll		= FALSE;
    
	
	if(!isset($_GET["CatID"]) || empty($_GET["CatID"])){
		$Categories			= $categoryController->FetchAllCategory();
		$displayAll = TRUE;
	}

?>

                <div class="container-fluid">
					<?php 
						
						if($displayAll){
							include_once("php/Includes/DisplayCategory.php");
						}else{
							include_once("php/Includes/EditCategory.php");
						}
					?>
                </div>
            </div>


<?php
include_once("footer.php");
?>