<?php
    date_default_timezone_set('Asia/Manila');

    include_once("header.php");
    include_once("php/functions.php");
    include_once("php/Control/LogsController.php");
    $logController = new LogsController();
    $hasNewNotif = $logController->fetchLogNotif();
    if($hasNewNotif){
        $logController->ClearComplainNotif();
    }

    $records = $search = $bymonth = "";



    $search = date("Y-m-d");
    if (isset($_GET["search"]) || !empty($_GET["search"])) {
        $search = $_GET["search"];
        if (isset($_GET["bymonth"]) && $_GET["bymonth"] != 0) {
            $bymonth = $_GET["bymonth"];
        }
        $records = $logController->fetchComplaintMsgDate($search, $bymonth);
    } else {
        $records =  $logController->fetchLatestComplain(5);
    }
    

?>


<div class="container-fluid">
    <div class="d-sm-flex justify-content-between align-items-center">
        <h3 class="text-dark mb-5">Search Complain</h3>
    </div>
    <div class=" mb-5">
        <form class="form" action="" method="get">
           
                    <input type="date" class="form-control w-25 mb-1" name="search" value="<?php echo $search;?>">
                
                    <div class="custom-control custom-checkbox mb-3">
                        <input type="checkbox" class="custom-control-input" id="customCheck1" name="bymonth" <?php returnChecked($bymonth,"1"); ?> value="1">
                        <label class="custom-control-label" for="customCheck1">Search by Month</label>
                    </div>
           
                
            <input class="btn btn-primary w-25" type="submit" value="Submit">
        </form>
    </div>
    <?php if (!isset($_GET["search"])) {
    echo "Latest 10 Complaints";
} ?>
    <table class="table">
  <thead>
    <tr>
      <th scope="col">Date</th>
      <th scope="col">Complainant (Contact)</th>
      <th scope="col">Shop</th>
      <th scope="col">View</th>
    </tr>
  </thead>
  <tbody>
    <?php
        foreach ($records as $record) { ?>
        <tr>
            <th scope="row"><?php echo $record["log_Date"]?></th>
            <td><?php echo $record["name"]." (".$record["contact"].")" ?></td>
            <td><?php echo $record["shop"];?></td>
            <td><a href="Readmsg.php?ID=<?php echo $record["ID"];?>">Read</a></td>
        </tr>   
    <?php
        }
    ?>
    
   
  </tbody>
</table>



    </div>
</div>







<?php
    include_once("footer.php");

?>