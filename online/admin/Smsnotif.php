<?php
    include_once("header.php");
    include_once("php/functions.php");

    include_once("php/Control/SMSController.php");
    //sendSms("Testing Lang Kung Gumagana");
    $smsController = new SMSController();

    $search = $add = $error = "";
    
    if(isset($_GET["delete"]) || !empty($_GET["delete"])){
        $delete = $_GET["delete"];
        if($smsController->DeleteNumber($delete)){
            //nothing
        }
        header("Location: Smsnotif.php");
        exit();
    }

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $required_number = 10;
        $passNumber = strlen($_POST["number"]);

        if(isset($_POST["number"]) || !empty($_POST["number"])){
            if($passNumber != $required_number){
                $error = "Contact number must be valid (10 Digit). Ex. 9123456789";
            }
            if($_POST["number"][0] != "9"){
                $error = "Invalid number (must start with 9.";
            }
            if($error == ""){
                $submitNum = "0".$_POST["number"];
                if($smsController->InsertNumber($submitNum)){
                    $add = "Successfully added.";
                }else{
                    $error = 'Failed to add.(Number already Exists)';
                }
            }
            
        }
    }


    if(isset($_GET["search"]) || !empty($_GET["search"])){
        $search = $_GET["search"];
    }
    $numbers = $smsController->FetchSearchNumbers($search);

?>

    <div class="container-fluid">
        <h3 class="text-dark mb-3">Mobile Numbers</h3>
        
        <?php showSuccessMsg($add);?>
        <?php showErrorMsg($error);?>
        <form class="form-inline" action="" method="post">
            <div class="form-group  mb-2 mr-2">
                <label for="number" class="sr-only">number</label>
                <input type="number" class="form-control" id="number" name="number" placeholder="9123456789">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Add Number</button>
        </form>
        
        <form class="form-inline mt-3 mb-5" action="" method="get">
            <div class="form-group  mb-2 mr-2">
                <label for="snumber" class="sr-only">number</label>
                <input type="number" class="form-control" id="snumber" name="search" placeholder="9123456789">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Search Number</button>
        </form>

        <div class="row">
            <?php 
            foreach ($numbers as $number) {
                
            ?>
            <div class="col-sm-6 col-md-4 col-lg-2 item">
                <table class="table w-25">
                    <tr class="border ">
                        <td><?php echo $number;?></td>
                        <td><a href="Smsnotif.php?delete=<?php echo $number;?>">Delete</a></td>
                    </tr>
                </table>
            </div>
            <?php }
            ?>

        </div>
    </div>


</div>
<?php 
    include_once("footer.php");

?>