<?php 
    session_start();
    include_once("php/functions.php");
    
    //Redirect to index.php if already logged in.
    checkIfAlreadyLogin();

    $error = array("username" => "", "password" => "");
    $username = $password = "";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        include_once("php/Control/LoginController.php");
        $loginController = new LoginController();

        //pass the User's input to variable
        $username = cleanInput($_POST["username"]);
        $password = cleanInput($_POST["password"]);
        
        //Check if the user's input is empty
        if(empty($username)){
            $error["username"] = "Username is empty.<br>";
        }
        if(empty($password)){
            $error["password"] = "Password is empty.<br>";
        }

        //if no error found proceed to check if the User's input is actually valid.
        if($error["username"] == "" && $error["password"] == ""){

            if($loginController->fetchUser($username,$password)){
                //Login Successfull
                $_SESSION['User'] = $username;
                header("location: index.php");
                exit();
            }else{
                $error["username"] = "Username or Password is Invalid.<br>";
            }
        }
        
    }
    
    
?>

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Daraga Market Price Monitoring System Admin Panel</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css?h=4d1e091e564b66954c885008dc1a7a9b">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
</head>



<body id="page-top">
    <div class="login-dark">
        <form method="post" action="login.php">
		     <h2><big><center><font color="blue">LOGIN</font><br></center></big></h2>
			<h2 class="text-center"><small>DARAGA PUBLIC MARKET PRICE MONITORING SYSTEM</small></h2>
            <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
            <div class="form-group">
                <?php 
                    if(isset($_GET["logout"])){
                        echo showSuccessMsg("Successfuly Log-out.<br>");
                    }
                ?>
                <?php echo showErrorMsg($error["username"]);?>
                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $username;?>"/>
            </div>
            <div class="form-group">
                <?php echo showErrorMsg($error["password"]);?>
                <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo $password;?>"/>
            </div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit">Log In</button></div>
        </form>
    </div>
</body>

</html>