<?php
    include_once("header.php");
    include_once("php/functions.php");
    include_once("php/Control/LogsController.php");
    $logController = new LogsController();

    if(!isset($_GET["ID"]) || empty($_GET["ID"])){
        header("Location: index.php");
        exit();
    }

    $ID = $_GET["ID"];
    $complain = $logController->fetchComplaintMessage($ID);

?>


<div class="container-fluid">
    <div class="d-sm-flex justify-content-between align-items-center">
        <h3 class="text-dark mb-0">Complaint Message for <span class="text-danger"><?php echo $complain["shop"]; ?></span></h3>
    </div>
    <small>Category: <?php echo $complain["category"]; ?></small><br>
    <small>Complainant: <?php echo $complain["name"]." (".$complain["contact"].")";?></small> <br>
    <small>Address: <?php echo $complain["address"];?></small> <br>
    <small class="mb-4"><?php echo $complain["log_Date"];?></small>
    <p class="mt-5"><?php echo $complain["message"];?></p>
    
</div>
</div>
<?php 
    include_once("footer.php");

?>