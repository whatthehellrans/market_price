<?php 
    include_once("header.php");
    if(!isset($_GET["catID"]))
    {
    	header("Location: index.php");
    	exit();
    }

    include_once("admin/php/Control/CategoryController.php");
	include_once("admin/php/Control/ProductController.php");

	$categoryController	= new CategoryController();
	$productController	= new ProductController();

	$category			= $categoryController->FetchSingleCategory($_GET["catID"]);
    $productArr			= $productController->FetchProductBaseOnCategory($category->getID());
    

?>


 <!-- PAGING FUNCTION -->
 <script  type="text/javascript">
        var current_page = 1;
        var records_per_page = 10;
        
        var items = [
            <?php
            foreach ($productArr as $prd) :  ?>
                { id: <?php echo $prd->getId(); ?>,name: "<?php echo $prd->getPrd_Name();?>"},
            <?php endforeach ?>
        ];
   
        function prevPage() {
            if (current_page > 1) {
                current_page--;
                changePage(current_page);
            }
        }

        function nextPage() {
            if (current_page < numPages()) {
                current_page++;
                changePage(current_page);
            }
        }

        function changePage(page) {
            var btn_next = document.getElementById("btn_next");
            var btn_prev = document.getElementById("btn_prev");
            var listingTable = document.getElementById("listingTable");
            var page_span = document.getElementById("page");
        
            // Validate page
            if (page < 1) page = 1;
            if (page > numPages()) page = numPages();

            listingTable.innerHTML = "";

            for (var i = (page-1) * records_per_page; i < (page * records_per_page) && i < items.length; i++) {
                tabBody=document.getElementsByTagName("tbody").item(0);
                row=document.createElement("tr");

                cell1 = document.createElement("td");
                cell3 = document.createElement("td");
                prdlink = document.createElement("a");

                textnode1=document.createTextNode(items[i].name);

                prdlink.innerHTML = "View Product";

                cell1.appendChild(textnode1);
                cell3.appendChild(prdlink);
                prdlink.setAttribute('href', 'Product.php?prdID='+ items[i].id);

                row.appendChild(cell1);
                row.appendChild(cell3);
                tabBody.appendChild(row);

            }

            page_span.innerHTML = page + "/" + numPages();

            if (page == 1) {
                //btn_prev.style.visibility = "hidden";
                btn_prev.classList.add("text-primary");
            } else {
                btn_prev.style.visibility = "visible";
                btn_prev.classList.remove("text-primary");
            }

            if (page == numPages()) {
                //btn_next.style.visibility = "hidden";
                btn_next.classList.add("text-primary");
            } else {
                btn_next.style.visibility = "visible";
                btn_next.classList.remove("text-primary");
            }
        }

        function numPages() {
            return Math.ceil(items.length / records_per_page);
        }
        window.onload = function() {
            changePage(1);
        }
    </script>


    <!-- Start: Projects Clean -->
    <div class="projects-clean">
        <div class="container">
            <!-- Start: Intro -->
            <div class="intro">
                <h2 class="text-center"><?php echo $category->getCat_Name();?></h2>
            </div>
            <!-- End: Intro -->
            <div class="container">
                <table class="table ">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                    <tbody id="listingTable">
                        <?php 
                            foreach ($productArr as $prd) { ?>
                                <tr>
                                    <th><?php echo $prd->getPrd_Name();
                                        if($prd->getPrd_Details() != "") echo " (". $prd->getPrd_Details().")";
                                    ?></th>
                                    <td><a href="Product.php?prdID=<?php echo $prd->getID();?>">View Product</a></td>
                                </tr>   
                        <?php
                            }
                        ?>
     
                    </tbody>
                </table>

                <a class="btn btn-primary" href="javascript:prevPage()" id="btn_prev">Prev</a>
                <a class="btn btn-primary" href="javascript:nextPage()" id="btn_next">Next</a>
                &nbsp page: <span id="page"></span>

            </div>
        </div>
    </div>
    <!-- End: Projects Clean -->
    
    <?php 
        include_once("footer.php");
    ?>