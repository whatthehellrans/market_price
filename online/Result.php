<?php
    include_once("header.php");
    include_once("admin/php/Control/CategoryController.php");
    include_once("admin/php/Control/ProductController.php");
    include_once("admin/php/Control/KeywordController.php");
    include_once("admin/php/functions.php");

    $categoryController = new CategoryController();
    $productController = new ProductController();
    $keywordController = new KeywordController();


    if (isset($_GET["search"])) {
        $search = cleanInput($_GET["search"]);
    } else {
        header("location: index.php");
        exit();
    }

    $categoryResult = $categoryController->FetchSearchCategory($search);
    $prdFromCategory = array();
    foreach ($categoryResult as $catID) {
        array_push($prdFromCategory, $productController->FetchProductBaseOnCategory($catID));
    }
    
    // echo "<pre>";
    // print_r($prdFromCategory);
    // echo "</pre>";

    
    $allresultprdID = array();
     //All Category Result Loop
     for ($i=0; $i < count($prdFromCategory); $i++) {
         //Products from each Category Loop
         for ($j=0; $j < count($prdFromCategory[$i]); $j++) {
             array_push($allresultprdID, $prdFromCategory[$i][$j]->getID());
         }
     }
    $prdResults = $productController->FetchSearchProduct($search);
    foreach ($prdResults as $prdSearch) {
        if (!in_array($prdSearch->getID(), $allresultprdID, true)) {
            array_unshift($allresultprdID, $prdSearch->getID());
        }
    }

    //Search From Keyword
    $kwSearch = $keywordController->FetchSearchKeyword($search);
    foreach ($kwSearch as $prdID) {
        if (!in_array($prdID, $allresultprdID, true)) {
            array_push($allresultprdID, $prdID);
        }
    }


    //Fetch object from db using Product ID
    $results = array();
    foreach ($allresultprdID as $products) {
        array_push($results, $productController->FetchSingleProduct($products));
    }


?>


    <!-- PAGING FUNCTION -->
    <script  type="text/javascript">
        var current_page = 1;
        var records_per_page = 10;
        
        var items = [
            <?php
            foreach ($results as $prd) :  ?>
                { id: <?php echo $prd->getId(); ?>,name: "<?php echo $prd->getPrd_Name();?>", category: "<?php echo $categoryController->FetchSingleCategory($prd->getPrd_Category())->getCat_Name();?>"},
            <?php endforeach ?>
        ];
   
        function prevPage() {
            if (current_page > 1) {
                current_page--;
                changePage(current_page);
            }
        }

        function nextPage() {
            if (current_page < numPages()) {
                current_page++;
                changePage(current_page);
            }
        }

        function changePage(page) {
            var btn_next = document.getElementById("btn_next");
            var btn_prev = document.getElementById("btn_prev");
            var listingTable = document.getElementById("listingTable");
            var page_span = document.getElementById("page");
        
            // Validate page
            if (page < 1) page = 1;
            if (page > numPages()) page = numPages();

            listingTable.innerHTML = "";

            for (var i = (page-1) * records_per_page; i < (page * records_per_page) && i < items.length; i++) {
                tabBody=document.getElementsByTagName("tbody").item(0);
                row=document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                prdlink = document.createElement("a");

                textnode1=document.createTextNode(items[i].name);
                textnode2=document.createTextNode(items[i].category);
                prdlink.innerHTML = "View Product";

                cell1.appendChild(textnode1);
                cell2.appendChild(textnode2);
                cell3.appendChild(prdlink);
                prdlink.setAttribute('href', 'Product.php?prdID='+ items[i].id);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                tabBody.appendChild(row);

            }

            page_span.innerHTML = page + "/" + numPages();

            if (page == 1) {
                //btn_prev.style.visibility = "hidden";
                btn_prev.classList.add("text-primary");
            } else {
                btn_prev.style.visibility = "visible";
                btn_prev.classList.remove("text-primary");
            }

            if (page == numPages()) {
                //btn_next.style.visibility = "hidden";
                btn_next.classList.add("text-primary");
            } else {
                btn_next.style.visibility = "visible";
                btn_next.classList.remove("text-primary");
            }
        }

        function numPages() {
            return Math.ceil(items.length / records_per_page);
        }
        window.onload = function() {
            changePage(1);
        }
    </script>



    <!-- Start: Projects Clean -->
    <div class="projects-clean">
        <div class="container">
            <!-- Start: Intro -->
            <div class="intro">
                <h2 class="text-center"><?php
                if (count($results) > 0) {
                    echo "SEARCH RESULT";
                } else {
                    echo "NO RESULT FOUND";
                }
                ?></h2>
            </div>
            <!-- End: Intro -->
            <!-- Start: Projects -->
            <?php if(count($results) > 0):?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Product</th>
                    </tr>
                </thead>
                <tbody  id="listingTable">
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <center><a class="btn btn-primary dark" href="javascript:prevPage()" id="btn_prev ">Prev</a>
            <a class="btn btn-primary" href="javascript:nextPage()" id="btn_next">Next</a>
            &nbsp page: <span id="page"></span></center>
            <?php endif ?>
            
           
        </div>
    </div>


  
        <?php
    include_once("footer.php");
?>