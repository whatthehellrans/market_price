<?php
	include_once("header.php"); //Include All HTML header

	include_once("admin/php/Control/ProductController.php");
	include_once("admin/php/Control/ImageController.php");
	include_once("admin/php/Model/Product.php");
	
	$productController	= new ProductController();
	$imageController	= new ImageController();
	$productArray		= $productController->FetchRandomProduct(8);


	
?>

	
<!-- End: Highlight Blue -->
<!-- Start: Lightbox Gallery -->
<div class="photo-gallery">
	<div class="container">
		<!-- Start: Intro -->
		<div class="intro">
			<h2 class="text-center">Product Gallery</h2>
			<h3 class="text-left"><small><small><small>“DPMPMS” is the Online Price Monitoring System (OPMS) of the DARAGA PUBLIC MARKET where consumers can 
			check the prevailing prices of basic necessities and prime commodities that are being monitored by the DTI.
			It serves as a price guide for consumers in doing their grocery shopping which in turn ensures “value for money."</small></small></small></h3>
		</div>
		<!-- End: Intro -->
		<!-- Start: Photos -->
		<div class="row photos">
			<?php 
					foreach ($productArray as $prd) {
					?>
						<div class="col-sm-6 col-md-4 col-lg-3 item text-center"><a data-lightbox="photos"
								href="admin/<?php echo $imageController->fetchSingleImage($prd->getID()); ?>"><img class="img-fluid"
									src="admin/<?php echo $imageController->fetchSingleImage($prd->getID()); ?>" style="height:12em;"></a>
							<p class="h4"><?php echo $prd->getPrd_Name(); ?></p>
							<p><?php echo $prd->getPrd_Details();?></p>
						</div>
			<?php	}
			?>
			
		</div>
		<!-- End: Photos -->
	</div>
</div>
<!-- End: Lightbox Gallery -->
<?php 
	include_once("footer.php");
?>