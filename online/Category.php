<?php

	if(!isset($_GET["cat"]) || empty($_GET["cat"])){
		header("Location: index.php");
		exit();
	}else{
		$categoryView = $_GET["cat"];
	}

    include_once("header.php"); //Include All HTML header

    include_once("admin/php/Control/ProductController.php");
    include_once("admin/php/Control/CategoryController.php");
    include_once("admin/php/Model/Category.php");
	
	$producController 	= new ProductController();
	$categoryController	= new CategoryController();
	$categoryArr		= $categoryController->FetchAllCategory();

	
?>

<!-- Start: Projects Clean -->
<div class="projects-clean">
    <div class="container">
        <!-- Start: Intro -->
        <div class="intro">
            <h2 class="text-center"><strong><?php if($categoryView == 1) echo "BASIC NECESSITIES"; else echo "PRIME COMMODITIES" ?></strong></h2>
            <p class="text-center"></p>
        </div>
        <!-- End: Intro -->
        <!-- Start: Projects -->
        <div class="container">
			<table class="table text-center">
				<tbody>
					<?php 
                    foreach ($categoryArr as $cat) {
                        $prd = $producController->FetchProductBaseOnCategory($cat->getID());
                        if ($cat->getPrimary_Category() == "Basic Necessities" && $categoryView == 1) {
                            if ($prd == null) {
                                continue;
                            } ?>
					<tr>
						<th><?php echo $cat->getCat_Name(); ?></th>
						<td><?php echo count($prd); ?> Items</td>
						<td><a href="CategoryView.php?catID=<?php echo $cat->getID(); ?>">Click Here to view</a></td>
					</tr><?php
                        } elseif ($cat->getPrimary_Category() == "Prime Commodities" && $categoryView == 2) {
                            if ($prd == null) {
                                continue;
                            } ?>
					<tr>
						<th><?php echo $cat->getCat_Name(); ?></th>
						<td><?php echo count($prd); ?> Items</td>
						<td><a href="CategoryView.php?catID=<?php echo $cat->getID(); ?>">Click Here to view</a></td>
					</tr><?php
                        }
					}
					
					?>
				</tbody>
			</table>
        </div>
        <!-- End: Projects -->
    </div>
</div>
<!-- End: Projects Clean -->

<?php 
	include_once("footer.php");
?>