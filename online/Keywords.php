<?php
    include_once("header.php");
    include_once("admin/php/functions.php");
    include_once("admin/php/Control/ProductController.php");
    include_once("admin/php/Control/KeywordController.php");
    include_once("admin/php/Control/CategoryController.php");
    
    $ProductController  = new ProductController();
    $KeywordController  = new KeywordController();
    $CategoryController = new CategoryController();
    
    if(!isset($_GET["search"]) || empty($_GET["search"])){
        $search = "";
    }else{
        $search = $_GET["search"];
    }

    $allproducts = array();
    //Search from Product Name
    $products = $ProductController->FetchSearchProduct($search);
    foreach ($products as $prd) {
        array_push($allproducts,$prd->getID());
    }
    
    //Search From Keyword
    $kwSearch = $KeywordController->FetchSearchKeyword($search);
    foreach ($kwSearch as $prdID) {
        //Merge search from product name and Keywords without duplicate.
        if(!in_array($prdID, $allproducts, true)){
            array_push($allproducts, $prdID);
        }
    }


    

?>
    <!-- Start: Projects Clean -->
    <div class="projects-clean">
        <div class="container">
            <!-- Start: Intro -->
            <div class="intro">
                <h2 class="text-center">Keyword Dictionary</h2>
            </div>
            <div class="text-center mb-5">
                <form class="form" action="" method="get">
                    <input type="text" class="form-control w-50 mb-3" style="margin:auto"name="search" placeholder="Search for Product or Keyword">
                    <input class="btn btn-primary w-25" type="submit" value="Submit">
                </form>
            </div>
            
            <!-- End: Intro -->
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Product</th>
                        <th scope="col">Category</th>
                        <th scope="col">Keywords</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach ($allproducts as $prd) { 
                        $Keywords = $KeywordController->FetchPrdKeyword($prd);
                        sort($Keywords);
                        if(!empty($Keywords)){
                            $Keywords = implode(", ",$Keywords);
                        }else{
                            $Keywords = "";
                        }
                        $prdobj = $ProductController->FetchSingleProduct($prd);
                        ?>
                        <tr>
                            <td><?php echo $prdobj->getPrd_Name(); if($prdobj->getPrd_Details() != "") echo " (".$prdobj->getPrd_Details().")";?></td>
                            <td><?php echo $CategoryController->FetchSingleCategory($prdobj->getPrd_Category())->getCat_Name();?></td>
                            <td><?php echo $Keywords?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    
                  
                </tbody>
            </table>
            
        </div>
    </div>
    <!-- End: Projects Clean -->
    <!-- TO DO -- add page
    <div class="resultspages">
        <p class="text-center">Showing Page 1 of 3</p><a class="pagesnumber" href="#">1</a><a class="pagesnumber" href="#">2</a>
        <a class="pagesnumber" href="#">3</a>
    </div> -->
        <?php 
	include_once("footer.php");
?>