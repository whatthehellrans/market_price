
let shopSelect = document.getElementById("market-shop");
let marketCategory = document.getElementById("market-category");
   


function ChangeSelection()
{
     
    let currentValue = marketCategory.value;
    let marketCategoryArr = {
        "Fish Section":         ["section 1","section 2"],
        "Dried Fish Section":   ["dried 1","dried 2"],
        "Meat Section":         ["meat 1","meat 2"],
        "Grocery Store Section":["store 1","store 2"],
        "Fruits Section":       ["Store 1","fruit 2"],
        
    }

    ClearShopInvolved();

    marketCategoryArr[currentValue].map(key =>{
        //console.log(key);
        UpdateShopInvolved(key);
    })
    

}

function UpdateShopInvolved(shopToAdd)
{
    //Update Using the Array that was pass.
    let shop = document.createElement("option");
    shop.text = shopToAdd;
    shop.value = shopToAdd;
    shopSelect.add(shop);
}

function ClearShopInvolved()
{
    //Clear Drop Down
    let shopLenght = shopSelect.options.length;
    for (let i = shopLenght - 1; i >= 0; i--) {
        shopSelect.options[i] = null;
    }
}

window.onload = function (){
    ChangeSelection();
}