<?php 
    include_once("header.php");

	if(!isset($_GET["prdID"]))
    {
    	header("Location: index.php");
    	exit();
    }
    include_once("admin/php/functions.php");

    include_once("admin/php/Control/ProductController.php");
    include_once("admin/php/Control/CategoryController.php");
	include_once("admin/php/Control/ImageController.php");
    include_once("admin/php/Control/KeywordController.php");
    include_once("admin/php/Control/LogsController.php");

	$productController	= new ProductController();
	$categoryController	= new CategoryController();
	$imageController	= new ImageController();
    $keywordController	= new KeywordController();
    $logsController     = new LogsController();

	$product	= $productController->FetchSingleProduct($_GET["prdID"]);

?>
    <!-- Start: Bold BS4 Portfolio Item Details Page -->
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-8">
                <div class="carousel slide" data-ride="carousel" id="carousel-1">
					<div class="carousel-inner" role="listbox">
                        <?php  
                            $Images = $imageController->fetchImagesOfProduct($product->getID());
                            for ($i=0; $i < count($Images); $i++) { ?>
                                <div style="" class="carousel-item  <?php if($i == 0) {echo "active";}?>"><img style="max-height:488px; margin:auto;"class="img-fluid w-100 d-block" src="admin/<?php echo $Images[$i]?>" alt="Slide Image"></div>
                       <?php
                       		}
                        ?>
                     
                    </div>
                    <div>
                        <!-- Start: Previous --><a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a>
                        <!-- End: Previous -->
                        <!-- Start: Next --><a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a>
                        <!-- End: Next -->
                    </div>
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-1" data-slide-to="1"></li>
                        <li data-target="#carousel-1" data-slide-to="2"></li>
                    </ol>
                </div>
            </div>
            <div class="col-md-4">
                <h3 class="my-3"><?php echo $product->getPrd_Name();?></h3>
				<p><?php echo $product->getPrd_Local();?></p>
				<p><?php echo $product->getPrd_Details();?></p>
				
                <h3 class="my-3"><span class="text-success">₱<?php echo $product->getPrd_Price();?></span> / <span class="text-info"><?php echo $product->getPrd_Unit();?></span></h3>
                
                <div>
                    <div class="table-responsive">
                <?php   $priceUpdates = $logsController->fetchPriceUpdatesOfProduct($product->getID(),4); 
                        if(count($priceUpdates) != 0):      ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Old Price</th>
                                    <!-- <th>New Price</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($priceUpdates as $update) { ?>
                                        <tr>
                                            <td><?php echo substr($update["log_Date"],2,9);?></td>
                                            <td>₱<?php echo $update["old_price"];?></td>
                                            <!-- <td  class="<?php echo priceUpdateColor($update["old_price"],$update["new_price"]);?>">₱<?php echo $update["new_price"];?></td> -->
                                        </tr>   
                                <?php
                                    }
                                ?>

                            </tbody>
                        </table>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <h3 class="my-4">Related Products<br></h3>
        <div class="row">
		<?php 
			$relatedProducts = $productController->FetchRelatedProduct($product->getPrd_Category(),$product->getID(),4);
			
			if($relatedProducts != NULL){
				foreach ($relatedProducts as $prd) {
					if($prd->getID() == $product->getID()){
						continue;
					}
					?>
					<div class="col-sm-6 col-md-3 mb-4"><a href="Product.php?prdID=<?php echo $prd->getID();?>">
						<img class="img-fluid" style="max-height:150px" src="admin/<?php echo $imageController->fetchSingleImage($prd->getID()); ?>"></a></div>
			<?php
					}	
			}
			?>			
			

			
        </div>
    </div>
    <!-- End: Bold BS4 Portfolio Item Details Page -->
   
<?php 

    include_once("footer.php");

?>